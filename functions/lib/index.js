"use strict";
// import * as functions from 'firebase-functions';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// // // Start writing Firebase Functions
// // // https://firebase.google.com/docs/functions/typescript
// //
// // export const helloWorld = functions.https.onRequest((request, response) => {
// //  response.send("Hello from Firebase!");
// // });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
// import { async } from 'rxjs/internal/scheduler/async';
// import { Order } from '../../src/app/shared/order';
admin.initializeApp();
exports.subscribeToTopic = functions.https.onCall((data, context) => __awaiter(this, void 0, void 0, function* () {
    yield admin.messaging().subscribeToTopic(data.token, data.topic);
    return `subscribed to ${data.topic}`;
}));
exports.unsubscribeFromTopic = functions.https.onCall((data, context) => __awaiter(this, void 0, void 0, function* () {
    yield admin.messaging().unsubscribeFromTopic(data.token, data.topic);
    return `unsubscribed from ${data.topic}`;
}));
exports.sendOnFirestoreUpdateOrderCooking = functions.firestore
    .document('orders/{orderId}')
    .onUpdate((snapshot) => __awaiter(this, void 0, void 0, function* () {
    const order = snapshot.after.data();
    console.log(order.orderStatus);
    if (order.orderStatus === 'cooking') {
        const restaurantId = order.restaurantID;
        // const customerId = order.customerID.slice(order.customerID.indexOf('+') + 1);
        console.log(restaurantId);
        console.log('entered inside the if statement');
        const notification = {
            title: 'طلب جديد',
            body: 'لقد وصل طلب جديد'
        };
        const payload = {
            notification,
            webpush: {
                notification: {
                    vibrate: [200, 100, 200],
                    icon: 'assets/icon/logo 205',
                }
            },
            topic: 'restaurant' + restaurantId
        };
        return admin.messaging().send(payload);
    }
    else {
        console.log('shit it is not working!');
        return false;
    }
}));
exports.sendOnFirestoreUpdateOrderDeliveryGuyArrived = functions.firestore
    .document('orders/{orderId}')
    .onUpdate((snapshot) => __awaiter(this, void 0, void 0, function* () {
    const order = snapshot.after.data();
    console.log(order.orderStatus);
    if (order.orderStatus === 'deliveryGuyArrived') {
        const restaurantId = order.restaurantID;
        console.log(restaurantId);
        console.log('entered inside the if statement');
        const notification = {
            title: 'وصل السائق',
            body: 'السائق وصل لمنطقة مطعمك لاستلام الطلب'
        };
        const payload = {
            notification,
            webpush: {
                notification: {
                    vibrate: [200, 100, 200],
                    icon: 'assets/icon/logo 205',
                }
            },
            topic: 'restaurant' + restaurantId
        };
        return admin.messaging().send(payload);
    }
    return false;
}));
exports.sendOnFirestoreCreateCustomerOrder = functions.firestore
    .document('orders/{orderId}')
    .onUpdate((snapshot) => __awaiter(this, void 0, void 0, function* () {
    const order = snapshot.after.data();
    console.log(order.orderStatus);
    if (order.orderStatus === 'arraivedToCustomer') {
        const customerId = order.customerID.slice(order.customerID.indexOf('+') + 1);
        const newCustomerId = customerId.replace('@', '~');
        // const restaurantId = order.restaurantID;
        console.log(customerId);
        console.log('entered inside the if statement');
        const notification = {
            title: 'وصل السائق',
            body: 'لقد وصل السائق الى منطقتك'
        };
        const payload = {
            notification,
            webpush: {
                notification: {
                    vibrate: [200, 100, 200],
                    icon: 'assets/icon/logo 205',
                }
            },
            topic: 'customer' + newCustomerId
        };
        return admin.messaging().send(payload);
    }
    return false;
}));
//# sourceMappingURL=index.js.map