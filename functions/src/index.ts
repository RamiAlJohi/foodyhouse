// import * as functions from 'firebase-functions';

// // // Start writing Firebase Functions
// // // https://firebase.google.com/docs/functions/typescript
// //
// // export const helloWorld = functions.https.onRequest((request, response) => {
// //  response.send("Hello from Firebase!");
// // });

import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
// import { async } from 'rxjs/internal/scheduler/async';
// import { Order } from '../../src/app/shared/order';

admin.initializeApp();

export const subscribeToTopic = functions.https.onCall(
    async (data, context) => {
        await admin.messaging().subscribeToTopic(data.token, data.topic);

        return `subscribed to ${data.topic}`;
    }
);

export const unsubscribeFromTopic = functions.https.onCall(
    async (data, context) => {
        await admin.messaging().unsubscribeFromTopic(data.token, data.topic);

        return `unsubscribed from ${data.topic}`;
    }
);



export const sendOnFirestoreUpdateOrderCooking = functions.firestore
.document('orders/{orderId}')
.onUpdate(async snapshot => {
    const order = snapshot.after.data();
    console.log(order.orderStatus);
    if (order.orderStatus === 'cooking') {
        const restaurantId = order.restaurantID;
        // const customerId = order.customerID.slice(order.customerID.indexOf('+') + 1);
        console.log(restaurantId);
        console.log('entered inside the if statement');
        const notification: admin.messaging.Notification = {
            title: 'طلب جديد',
            body: 'لقد وصل طلب جديد'
        };
    
        const payload: admin.messaging.Message = {
            notification,
            webpush: {
                notification: {
                vibrate: [200, 100, 200],
                icon: 'assets/icon/logo 205',
                // actions: [
                //     {
                //     action: 'like',
                //     title: '👍 Yaaay!'
                //     },
                //     {
                //     action: 'dislike',
                //     title: 'Boooo!'
                //     }
                // ]
                }
            },
            topic: 'restaurant' + restaurantId
        };
    
        return admin.messaging().send(payload);
    } else {
        console.log('shit it is not working!');
        return false;
    }
});


export const sendOnFirestoreUpdateOrderDeliveryGuyArrived = functions.firestore
.document('orders/{orderId}')
.onUpdate(async snapshot => {
    const order = snapshot.after.data();
    console.log(order.orderStatus);
    if (order.orderStatus === 'deliveryGuyArrived') {
        const restaurantId = order.restaurantID;
        console.log(restaurantId);
        console.log('entered inside the if statement');
        const notification: admin.messaging.Notification = {
            title: 'وصل السائق',
            body: 'السائق وصل لمنطقة مطعمك لاستلام الطلب'
        };
    
        const payload: admin.messaging.Message = {
            notification,
            webpush: {
                notification: {
                vibrate: [200, 100, 200],
                icon: 'assets/icon/logo 205',
                // actions: [
                //     {
                //     action: 'like',
                //     title: '👍 Yaaay!'
                //     },
                //     {
                //     action: 'dislike',
                //     title: 'Boooo!'
                //     }
                // ]
                }
            },
            topic: 'restaurant' + restaurantId
        };
    
        return admin.messaging().send(payload);
    } 
    return false;
});

export const sendOnFirestoreCreateCustomerOrder = functions.firestore
.document('orders/{orderId}')
.onUpdate(async snapshot => {
    const order = snapshot.after.data();
    console.log(order.orderStatus);
    if (order.orderStatus === 'arraivedToCustomer') {
        const customerId = order.customerID.slice(order.customerID.indexOf('+') + 1);
        const newCustomerId = customerId.replace('@', '~');
        // const restaurantId = order.restaurantID;
        console.log(customerId);
        console.log('entered inside the if statement');
        const notification: admin.messaging.Notification = {
            title: 'وصل السائق',
            body: 'لقد وصل السائق الى منطقتك'
        };
    
        const payload: admin.messaging.Message = {
            notification,
            webpush: {
                notification: {
                vibrate: [200, 100, 200],
                icon: 'assets/icon/logo 205',
                // actions: [
                //     {
                //     action: 'like',
                //     title: '👍 Yaaay!'
                //     },
                //     {
                //     action: 'dislike',
                //     title: 'Boooo!'
                //     }
                // ]
                }
            },
            topic: 'customer' + newCustomerId
        };
    
        return admin.messaging().send(payload);
    } 
    return false;
});
