// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// export const environment = {
//   production: false
// };

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCX2SvEaSMg1xCms3LsLBfOkR0xlcxe-X4',
    authDomain: 'foodhouse-47e0c.firebaseapp.com',
    databaseURL: 'https://foodhouse-47e0c.firebaseio.com',
    projectId: 'foodhouse-47e0c',
    storageBucket: 'foodhouse-47e0c.appspot.com',
    messagingSenderId: '399094316305'
  }
};
