import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentChangeAction } from '@angular/fire/firestore';
import { Chief } from '../shared/chief';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Item } from '../shared/item';
import { Location } from '../shared/location';
import { Restaurant } from '../shared/restaurant';
import { Customer } from '../shared/customer';
import { Order } from '../shared/order';
import { OrderItem } from '../shared/orderItem';
import { Notification } from '../shared/notification';


@Injectable({
  providedIn: 'root'
})
export class ChiefService {
  userEmail: string;
  chiefCol: AngularFirestoreCollection<Chief>;

  constructor(private afs: AngularFirestore) { }

  getChief(id: string): Observable<Chief> {
    this.chiefCol = this.afs.collection('chiefs');
    return this.chiefCol.doc<Chief>(id).valueChanges();
  }

  getChiefs(): Observable<Chief[]> {
    this.chiefCol = this.afs.collection('chiefs');
    return this.chiefCol.snapshotChanges().pipe(map(actions => actions.map( action => {
        const data = action.payload.doc.data() as Chief;
        const id = action.payload.doc.id;
        return {id, ...data};
      })));
  }

  getCustomerValue(customerID) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).valueChanges();
  }

  getCustomer(customerID) {
    return this.afs.collection<Customer>('customers').doc(customerID).snapshotChanges().pipe(map(action => {
      const data = action.payload.data() as Customer;
      const id = action.payload.id;
      return {id, ...data};
    }));
  }

  getCustomers() {
    return this.afs.collection<Customer>('customers').snapshotChanges();
  }

  getRestaurantsValue() {
    return this.afs.collection<Customer>('restaurants').valueChanges();
  }

  getCustomersChanges() {
    return this.afs.collection<Customer>('customers').snapshotChanges().pipe(map(actions => actions.map( action => {
      const data = action.payload.doc.data() as Customer;
      const id = action.payload.doc.id;
      return {id, ...data};
    })));
  }

  addCustomer(customer: Customer, id: string) {
    this.afs.collection<Customer>('customers').doc(id).set(customer);
    return this.afs.collection<Customer>('customers').doc<Customer>(id);
  }

  getOrders(restaurantID) {
    return this.afs.collection<Order>('orders', ref => ref.where('restaurantID', '==', restaurantID)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Order;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
  updateOrder(orderID, data) {
    return this.afs.collection<Order>('orders').doc<Customer>(orderID).update(data);
  }

  getOrderItems(orderID) {
    return this.afs.collection('orders').doc<Order>(orderID).collection<OrderItem>('orderItems').valueChanges();
  }

  newChief(chief: Chief, id: string) {
    this.chiefCol = this.afs.collection('chiefs');
    this.chiefCol.doc(id).set(chief);
    return this.chiefCol.doc<Chief>(id);
  }

  updateCustomer(data, phoneNumber) {
    return this.afs.collection<Customer>('customers').doc(phoneNumber).update(data);
  }

  updateChief(data, phoneNumber) {
    this.chiefCol = this.afs.collection('chiefs');
    this.chiefCol.doc(phoneNumber).update(data);
  }

  getItem(restaurantID, itemID) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID)
                    .collection<Item>('items').doc<Item>(itemID).snapshotChanges();
  }

  addItem(item: Item, restaurantId) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantId).collection<Item>('items').add(item);
  }

  updateItem(item, restaurantID, itemID) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID).collection<Item>('items').doc(itemID).update(item);
  }

  deleteItem(restaurantID, itemID) {
    return this.afs.collection<Restaurant>('restaurants').doc(restaurantID).collection<Item>('items').doc(itemID).delete();
  }


  getNotification(restaurantID: string) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID).collection<Notification>('notifications')
    .valueChanges();
  }

  addNotify(restaurantID: string, notify: Notification) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID).collection<Notification>('notifications')
    .add(notify);
  }
}
