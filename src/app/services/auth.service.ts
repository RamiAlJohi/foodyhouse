import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { first } from 'rxjs/operators';
import { resolve } from 'url';
import { ToastController } from '@ionic/angular';
import { FcmService } from './fcm.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth, private toastCtrl: ToastController, private fcm: FcmService) { }

  login(value) {
    return new Promise<any>( (resolve, reject) => {
      console.log(value.email);
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }

  loginErrors(err) {
    console.log(err);
    switch (err.code) {
      case 'auth/invalid-email' : {
        this.presentToast('لقد ادخلت ايميل خاطئ');
      }
      break;
      case 'auth/user-disabled' : {
        this.presentToast('المسنخدم الذي تحاول الدخول به تم اقفه');
      }
      break;
      case 'auth/user-not-found' : {
        this.presentToast('المسنخدم الذي تحاول الدخول به غير متوفر');
      }
      break;
      case 'auth/wrong-password' : {
        this.presentToast('كلمة المرور غير صحيحة');
      }
      break;
      default: this.presentToast(err.message);
    }
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss().then(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  register(email, password) {
    const actionCodeSettings = {
      url: 'http://localhost:8100/home',
      iOS: {
        bundleId: 'com.foodyHouse.ios'
      },
      android: {
        packageName: 'com.foodyHouse.android',
        installApp: false,
        minimumVersion: '12'
      },
      handleCodeInApp: true
    };
    return new Promise<any>( (resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
              .then( res => {
                res.user.sendEmailVerification(actionCodeSettings).then( success => {
                  console.log('an email has been sent!');
                  this.presentToast('تم ارسال رسالة الى هذاالبريد الالكتروني ' + res.user.email);
                })
                .catch( err => {console.log(err.message); });
                resolve(res);
              }, err => reject(err));
    });
  }

  resetPassword(email) {
    const actionCodeSettings = {
      url: 'http://localhost:8100/home',
      iOS: {
        bundleId: 'com.foodyHouse.ios'
      },
      android: {
        packageName: 'com.foodyHouse.android',
        installApp: false,
        minimumVersion: '12'
      },
      handleCodeInApp: true
    };
    return new Promise<any>( (resolve, reject) => {
      firebase.auth().sendPasswordResetEmail(email, actionCodeSettings)
        .then( res => {
           console.log('email reset has been sent');
           resolve(res);
        }, err => reject(err));
        // .catch( err => {console.log(err.message);});
    });
  }
  
  registerErrors(err) {
    switch (err.code) {
      case 'auth/email-already-in-use' : {
        this.presentToast('الايميل هذا مستخدم من قبل');
      }
      break;
      case 'auth/invalid-email' : {
        this.presentToast('هذا الايميل غير صالح');
      }
      break;
      case 'auth/operation-not-allowed' : {
        this.presentToast('هذي العملية غير مقبولة');
      }
      break;
      case 'auth/weak-password' : {
        this.presentToast('كلمة السر ضعيفة');
      }
      break;
      default: this.presentToast(err.message);
    }
  }

  forgetPassword(email: string) {
    this.afAuth.auth.sendPasswordResetEmail(email)
      .then( success => {
        console.log('a reset email has been successfully sent!');
      });
  }

  forgetPasswordError(err) {
    switch (err.code) {
      case 'auth/invalid-email' : {
        this.presentToast('هذا الايميل غير صالح');
      }
      break;
      case 'auth/user-not-found' : {
        this.presentToast('لم ينم العثور على المستخدم');
      }
      break;
      default: this.presentToast(err.message);
    }
  }

  getUser() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  // getUserByEmail(email: string) {
  //   return this.afAuth.auth.;
  // }

  getUserOservable() {
    return this.afAuth.authState.pipe(first());
  }

  userSignOut() {
    console.log('logging out');
    return this.afAuth.auth.signOut();
  }
}
