import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Restaurant } from '../shared/restaurant';
import { Location } from '../shared/location';
import { Day } from '../shared/Day';
import { Review } from '../shared/review';
import { Item } from '../shared/item';
import { Time } from '@angular/common';
import { map, first, switchMap, last } from 'rxjs/operators';
import { Customer } from '../shared/customer';
import { PhoneNumber } from '../shared/phoneNumber';
import { Order, ORDERSTATUS } from '../shared/order';
import { OrderItem } from '../shared/orderItem';
import { AuthService } from './auth.service';
import { Notification } from '../shared/notification';

// import firebase = require('firebase');
// import * as firebase from '@an';

@Injectable({
  providedIn: 'root'
})
export class RestaurantListService {

  constructor(private afs: AngularFirestore) { }

  getRestaurantsSnap(limit) {
    return this.afs.collection<Restaurant>('restaurants', ref => ref.orderBy('name').limit(limit) ).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Restaurant;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  getRestaurantSnap(restaurnatID: string) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurnatID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Restaurant;
        const id = restaurnatID; // a.payload.id;
        return { id, ...data };
      })
    );
  }

  getItemsSnap(restaurnatID: string) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurnatID).collection<Item>('items').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Item;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  getItemSnap(restaurnatID: string, itemID: string) {
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurnatID).collection<Item>('items').doc<Item>(itemID)
    .snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Item;
        const id = a.payload.id;
        return { id, ...data };
      })
    );
  }

  getCommentsVal(restaurnatID: string) {
    const commentsCollection = this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurnatID).collection<Review>('reviews');
    return commentsCollection.valueChanges();
  }

  getNumberOfReviews(restaurnatID: string) {
    const commentsCollection = this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurnatID).collection<Review>('reviews');
    return commentsCollection.valueChanges().pipe(map(reviews => reviews.length));
  }

  getCustomer(customerID: string) {
    const customerCol = this.afs.collection('customers');
    return customerCol.doc<Customer>(customerID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Customer;
        const id = customerID; // a.payload.id;
        return { id, ...data };
      })
    );
  }

  getOrderSnap(orderID: string) {
    return this.afs.collection('orders').doc<Order>(orderID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Order;
        const id = a.payload.id;
        return { id, ...data };
      })
    );
  }

  getOrders(customerID: string) {
    return this.afs.collection<Order>('orders',
    ref => ref.where('customerID', '==', customerID)
    // .where('orderStatus', '==', ORDERSTATUS[1])
    )
    .snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Order;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  getOrderItems(orderID) {
    return this.afs.collection('orders').doc<Order>(orderID).collection<OrderItem>('orderItems').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as OrderItem;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }


  addReview(restaurantID: string, review: Review) {
    const commentsCollection = this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID).collection<Review>('reviews');
    return commentsCollection.add(review);
  }

  createOrderForSession(customerID: string, restaurantID: string) {
    const emptyOrder: Order = {
      restaurantID: restaurantID,
      customerID: customerID,
      orderStatus: ORDERSTATUS[0]
    };
    return this.afs.collection<Order>('orders').add(emptyOrder);
  }

  assignRestaurantIDToSession(orderingSessionID: string, restaurantID: string) {
    return this.afs.collection<Order>('orders').doc(orderingSessionID).update({restaurantID: restaurantID, orderStatus: ORDERSTATUS[0]});
  }

  deleteOrderItems(orderingSessionID: string) {
    return this.afs.collection<Order>('orders').doc(orderingSessionID).collection('orderItems').snapshotChanges().pipe(first(),
      map(actions => actions.map(a => {
        const id = a.payload.doc.id;
        console.log('try to delete' + id);
        return this.afs.collection<Order>('orders').doc(orderingSessionID).collection('orderItems').doc(id).delete()
        .then(aa => console.log(id + ', item deleted'));
      }))
    ).toPromise();
  }

  releseSession(customerID: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({orderingSessionID: null});
  }

  // addOrderItem(customerID: string, restaurnatID: string, ItemID: string) { // old
  //   console.log(1);
  //   this.getItemSnap(restaurnatID, ItemID)
  //   .pipe(first()).toPromise().then(item => {
  //     console.log(2);
  //     const orderItem: OrderItem = {item: item, quantity: 1};
  //     this.getCustomer(customerID).pipe(first()).toPromise().then(customer => {
  //       console.log(3);
  //       this.afs.collection('orders').doc<Order>(customer.orderingSessionID).collection<OrderItem>('orderItems').add(orderItem);
  //     });
  //   });
  // }

  addOrderItemToSession(orderingSessionID: string, item: Item) {
    const orderItem: OrderItem = {item: item, quantity: 1};
    console.log(orderItem);
    return this.afs.collection('orders').doc<Order>(orderingSessionID).collection<OrderItem>('orderItems').add(orderItem);
  }

  updateQuantity(orderingSessionID: string, orderItemID: string, quantity: number) {
    return this.afs.collection('orders').doc<Order>(orderingSessionID).collection<OrderItem>('orderItems')
    .doc(orderItemID).update({quantity: quantity});
  }

  deleteOrderItem(orderingSessionID: string, orderItemID: string) {
    return this.afs.collection('orders').doc<Order>(orderingSessionID).collection<OrderItem>('orderItems').doc(orderItemID).delete();
  }

  updateOrderStatus(orderID: string, orderStatus: string) {
    return this.afs.collection('orders').doc<Order>(orderID).update({orderStatus: orderStatus});
  }

  updateOrderSessionID(customerID: string, orderSessionID: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({orderingSessionID: orderSessionID});
  }

  updateCustomerLocation(customerID: string, location: Location) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({location: location});
  }

  updateCustomerFirstName(customerID: string, firstName: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({firstName: firstName});
  }
  updateCustomerLastName(customerID: string, lastName: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({lastName: lastName});
  }
  updateCustomerMobile(customerID: string, mobile: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({mobile: mobile});
  }
  updateCustomerEmail(customerID: string, email: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).update({email: email});
  }

  // getRstaurnatRates(restaurantID: string) {
  //   return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID)
  //   .collection<Review>('reviews').valueChanges()
  //   .pipe(map(reviews => reviews.map(review => review.rate)));
  // }

  updateRating(restaurantID: string, rate: number) {
    console.log(rate);
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurantID).update({rate: rate});
  }

  getNotification(customerID: string) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).collection<Notification>('notifications')
    .valueChanges();
  }

  addNotify(customerID: string, notify: Notification) {
    return this.afs.collection<Customer>('customers').doc<Customer>(customerID).collection<Notification>('notifications')
    .add(notify);
  }

}
