import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DeliveryGuy } from '../shared/deliveryguy';
import { map, tap, count, first } from 'rxjs/operators';
import { Order, ORDERSTATUS } from '../shared/order';
import { Restaurant } from '../shared/restaurant';
import { OrderItem } from '../shared/orderItem';
import { Customer } from '../shared/customer';
import { Delivery, DELIVERYSTATUS } from '../shared/delivery';
import { Observable } from 'rxjs';
import { Notification } from '../shared/notification';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(private afs: AngularFirestore) { }

  getDeliveryGuys() {
    return this.afs.collection('deliveryGuys').snapshotChanges();
  }

  getDeliveryGuy(deliveryGuyID: string) {
    return this.afs.collection('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as DeliveryGuy;
        const id = deliveryGuyID; // a.payload.id;
        return { id, ...data };
      })
    );
  }

  getDeliveryGuyChanges() {
    return this.afs.collection<Customer>('deliveryGuys').snapshotChanges().pipe(map(actions => actions.map( action => {
      const data = action.payload.doc.data() as DeliveryGuy;
      const id = action.payload.doc.id;
      return {id, ...data};
    })));
  }

  getDeliveryGuyValue(phoneNumber: string): Observable<DeliveryGuy> {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(phoneNumber).valueChanges();
  }
  getDeliveryTest(phoneNumber: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(phoneNumber).snapshotChanges();
  }
  addDeliveryGuy(deliveryGuy, email) {
    this.afs.collection<DeliveryGuy>('deliveryGuys').doc(email).set(deliveryGuy);
    return this.afs.collection<Customer>('deliveryGuys').doc<DeliveryGuy>(email);
  }

  updateDeliveryGuy(deliveryGuyID, data) {
    this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).update(data);
  }

  getCompletedOrders(dataLimit) {
    return this.afs.collection<Order>('orders', ref => {
      return ref.where('orderStatus', '==', ORDERSTATUS[2]).limit(dataLimit);
    })
    .snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Order;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  getOrderSize() {
    return this.afs.collection<Order>('orders', ref => ref.where('orderStatus', '==', ORDERSTATUS[2]))
            .valueChanges();
  }

  getRestaurantSnap(restaurnatID: string) { // CUST
    return this.afs.collection<Restaurant>('restaurants').doc<Restaurant>(restaurnatID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Restaurant;
        const id = restaurnatID; // a.payload.id;
        return { id, ...data };
      })
    );
  }

  getOrderItems(orderID) { // CUST
    return this.afs.collection('orders').doc<Order>(orderID).collection<OrderItem>('orderItems').valueChanges();
  }

  getCustomer(customerID: string) { // cust
    const customerCol = this.afs.collection('customers');
    return customerCol.doc<Customer>(customerID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Customer;
        const id = customerID; // a.payload.id;
        return { id, ...data };
      })
    );
  }

  updateOrderStatus(orderID: string, orderStatus: string) {
    return this.afs.collection<Order>('orders').doc<Order>(orderID).update({orderStatus: orderStatus});
  }

  updateDeliveryStatus(deliveryID: string, deliveryStatus: string) {
    return this.afs.collection<Delivery>('deliveries').doc<Delivery>(deliveryID).update({deliveryStatus: deliveryStatus});
  }

  addDelivery(delivery: Delivery) {
    return this.afs.collection<Delivery>('deliveries').add(delivery);
  }

  assignDelivery(deliveryGuyID: string, deliveryID: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).update({deliveryID: deliveryID});
  }

  getDelivery(deliveryID: string) {
    return this.afs.collection<Delivery>('deliveries').doc<Delivery>(deliveryID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Delivery;
        const id = a.payload.id;
        return { id, ...data };
      })
    );
  }

  getOrderSnap(orderID: string) {
    return this.afs.collection('orders').doc<Order>(orderID).snapshotChanges().pipe(
      map(a => {
        const data = a.payload.data() as Order;
        const id = a.payload.id;
        return { id, ...data };
      })
    );
  }
  getDeliveryGuyDeliveries(deliveryGuyID: string) {
    return this.afs.collection<Delivery>('deliveries', ref =>
    ref.where('deliveryGuyID', '==', deliveryGuyID)
    .where('deliveryStatus', '==', DELIVERYSTATUS[4])).valueChanges();
  }

  freeDeliveryGuySession(deliveryGuyID: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).update({deliveryID: null});
  }

  // profile
  updateDeliveryGuyFirstName(deliveryGuyID: string, firstName: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).update({firstName: firstName});
  }
  updateDeliveryGuyLastName(deliveryGuyID: string, lastName: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).update({lastName: lastName});
  }
  updateDeliveryGuyMobile(deliveryGuyID: string, mobile: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).update({mobile: mobile});
  }

  getNotification(deliveryGuyID: string) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).collection<Notification>('notifications')
    .valueChanges();
  }

  addNotify(deliveryGuyID: string, notify: Notification) {
    return this.afs.collection<DeliveryGuy>('deliveryGuys').doc<DeliveryGuy>(deliveryGuyID).collection<Notification>('notifications')
    .add(notify);
  }

  // getDeliveryGuyProfit(deliveryGuyID: string) {
  //   return this.afs.collection<Delivery>('deliveries', ref =>
  //   ref.where('deliveryGuyID', '==', deliveryGuyID)
  //   .where('deliveryStatus', '==', DELIVERYSTATUS[4])).valueChanges();
  // }

}
