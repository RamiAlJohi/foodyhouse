import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  isLoading = false;
  constructor(public loadingCtrl: LoadingController) { }

  async present(time) {
    this.isLoading = true;
    const loader = await this.loadingCtrl
                          .create({
                            duration: time,
                            message: 'انتظر... '
                          });
      await loader.present();
      this.isLoading = false;
  }

  dismiss() {
    console.log('loading service suppose to work!');
    // this.isLoading = false;
    if (this.isLoading) {
      this.loadingCtrl.dismiss();
    }
  }

}
