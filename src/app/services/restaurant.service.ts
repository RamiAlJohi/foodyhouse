import { Injectable } from '@angular/core';
import { Restaurant } from '../shared/restaurant';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  restaurantCol: AngularFirestoreCollection<Restaurant>;

  constructor(private afs: AngularFirestore) { }

  newRestaurant(restaurant: Restaurant) {
    this.restaurantCol = this.afs.collection('restaurants');
    return this.restaurantCol.add(restaurant);
  }

  getRestaurantInfo(userPhoneNumber): Observable<Restaurant> {
    this.restaurantCol = this.afs.collection('restaurants');
    return this.restaurantCol.doc<Restaurant>(userPhoneNumber).snapshotChanges().pipe( map( action => {
      const data = action.payload.data() as Restaurant;
      const id = action.payload.id;
      return {id, ...data};
    }));
  }

  updateRestaurant(data, restaurantID) {
    this.restaurantCol = this.afs.collection('restaurants');
    return this.restaurantCol.doc<Restaurant>(restaurantID).update(data);
  }
}
