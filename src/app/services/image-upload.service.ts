import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Injectable({
  providedIn: 'root'
})
export class ImageUploadService {

  imageURI: any;
  imageFileName: any = '';


  constructor(private storage: AngularFireStorage, private camera: Camera) {
    console.log('make sure this part run');
    this.storage.ref('No_Image_Available.png').getDownloadURL().subscribe(downloadUrl => {
      this.imageFileName = downloadUrl;
    });
  }

  async getImage() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };
    await this.camera.getPicture(options).then( result => {
      this.imageURI = 'data:image/jpeg;base64,' + result;
      this.imageFileName = this.imageURI;
    }, (err) => { console.log(err); });
  }

  async uploadFile(path) {
    if (this.imageURI === '') {
      return this.imageFileName;
    }
    const filePath = this.storage.ref('pictures/' + path);

    return await filePath.putString(this.imageURI, 'data_url');
  }

  // uploadFileErrors(err) {
  //   switch(err.code) {
  //     case ''
  //   }
  // }
}
