import { TestBed, inject } from '@angular/core/testing';

import { ChiefService } from './chief.service';

describe('ChiefService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChiefService]
    });
  });

  it('should be created', inject([ChiefService], (service: ChiefService) => {
    expect(service).toBeTruthy();
  }));
});
