import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { AngularFireFunctions } from '@angular/fire/functions';
import { ToastController, Platform } from '@ionic/angular';
import { tap } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Firebase } from '@ionic-native/firebase/ngx';
import * as app from 'firebase';
import { from } from 'rxjs';
// Fixing temporary bug in AngularFire


@Injectable({
  providedIn: 'root'
})
export class FcmService {
  token;

  constructor(private afMessaging: AngularFireMessaging,
              private fun: AngularFireFunctions,
              private afs: AngularFirestore,
              private firebaseNative: Firebase,
              private platform: Platform,
              private toastController: ToastController
            ) {
                  try {
                      const _messaging = app.messaging();
                      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
                      _messaging.onMessage = _messaging.onMessage.bind(_messaging);
                    } catch (e) { }
    }
  async makeToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 5000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'dismiss'
    });
    toast.present();
  }

  getPremission() {
    console.log('wtf');
    let token$;
    if (this.platform.is('cordova')) {
      token$ = from(this.getPremissionNative());
    } else {
      token$ = this.getPremissionWeb();
    }
    return token$.pipe(tap(token => {
      console.log(token);
      this.token = token;
    }));
    // return this.afMessaging.requestToken.pipe(tap(token => {this.token = token; console.log(token); } ));
  }

  getPremissionWeb() {
    return this.afMessaging.requestToken;
  }

  private async getPremissionNative() {
    let token;

    if (this.platform.is('ios')) {
      await this.firebaseNative.grantPermission();
    }
    token = await this.firebaseNative.getToken();

    return token;
  }

  listenToMessages() {
    let messages$;
    if (this.platform.is('cordova')) {
      messages$ = this.firebaseNative.onNotificationOpen();
    } else {
      messages$ = this.afMessaging.messages;
    }

    return messages$.pipe(tap(v => this.showMessages(v)));
  }

  showMessages(payload) {
    let body;
    if (this.platform.is('android')) {
      body = payload.body;
    } else {
      body = payload.notification.body;
    }
    this.makeToast(body);
    // return this.afMessaging.messages.pipe(tap(msg => {
    //   const body: any = (msg as any).notification.body;
    //   this.makeToast(body);
    // }));
  }
  updateAt(collectionName, data) {
    this.afs.collection(collectionName).add(data);
  }

  sub(topic) {
    console.log(topic);
    this.fun
      .httpsCallable('subscribeToTopic')({topic, token: this.token})
      .pipe(tap( () => (console.log(topic))))
      .subscribe();
  }

  unsub(topic) {
    this.fun
      .httpsCallable('unsubscibeFromTopic')({topic, token: this.token})
      .subscribe();
  }
}
