import { OrderItem } from './orderItem';
import { Location } from './location';


export class OrderInfo {
    orderID: string;
    restaurantName?: string;
    customerName?: string;
    cutsomerPhoneNumber?: string;
    orderItems?: OrderItem[];
    totalPrice?: number;
    restImage?: string;
    customerLocation?: Location;
    restaurantLocation?: Location;
    distanceToRestaurant?: number;
    distanceToCustomer?: number;
    deliveryPrice?: number;
  }
