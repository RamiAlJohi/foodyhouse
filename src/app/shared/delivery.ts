import { Location } from './location';

export class Delivery {
    id?: string;
    orderID: string;
    deliveryStatus: string;
    deliveryGuyID: string;
    deliveryFees: number;
    customerLocation: Location;
    restaurantLocation: Location;
}

export const DELIVERYSTATUS: string[] = ['accepted', 'arrivedRestaurant', 'picked', 'arrivedCustomer', 'done']; // removed status a status
