import { Day } from './Day';
import { Location } from './location';

export class Restaurant {
    id?: string;
    name: string;
    description: string;
    rate: number;
    averagePreparationTime: number;
    image: string;
    workingHours: {
        sunday:  Day,
        monday: Day,
        tuesday: Day,
        wensday: Day,
        thursday: Day,
        friday: Day,
        saturday: Day,
    };
    location: Location;
    restaurantStatus: string; // fixed
    categories: string[];
    numberOfRates?: number;
}

export const restStatus: string[] = ['open', 'busy', 'close'];
