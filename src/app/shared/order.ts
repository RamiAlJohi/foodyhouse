import { OrderInfo } from './orderInfo';

export class Order {
    id?: string;
    restaurantID: string;
    customerID: string;
    // orderItemsID?: string; // sub collection
    orderStatus: string;
    orderInfo?: OrderInfo;
    // customerLocation: Location; // it will be a problem if it changed XD
    // restaurantLocation: Location; // it will be a problem if it changed XD
}

export const ORDERSTATUS: string[] =
    ['ordering', 'cooking', 'completed', 'deliveryAccepted', 'deliveryGuyArrived', 'picked', 'arraivedToCustomer', 'receivedByCustomer'];
