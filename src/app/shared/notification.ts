import { Time } from '@angular/common';

export class Notification {
    headline: string;
    date: string;
}
