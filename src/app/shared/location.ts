export class Location {
    lat: number;
    lng: number;
    city?: string;
    address?: string;
    buildingNumber?: string;
    apartmentNumber?: string;
    isActive: boolean;
}
