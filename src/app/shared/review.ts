export class Review {
    name: string;
    date: string;
    rate: number;
    comment?: string;
}
