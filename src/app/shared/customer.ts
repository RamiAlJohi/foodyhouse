import { Location } from './location';
import { Order } from './order';
import { PhoneNumber } from './phoneNumber';

export class Customer {
    id?: string;
    firstName: string;
    lastName: string;
    mobile: string; // tostring
    email: string;
    image?: string; // test page without image
    location: Location;
    orderingSessionID?: string; // optional
}
