export class Item {
    id?: string;
    name: string;
    price: number;
    description: string;
    image: string;
    category?: string;
}

export const CATEGORIES: string[] = ['food', 'drink', 'dessert', 'appetizer'];
export const CATEGORIESTEXT: string[] = ['وجبات', 'مشروبات', 'تحلية', 'مقبلات'];
