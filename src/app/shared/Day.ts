import { Time } from '@angular/common';

export class Day {
    isOpen: string;
    from: string;
    to: string;
}
