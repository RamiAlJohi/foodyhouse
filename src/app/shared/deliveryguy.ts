import { Notification } from './notification';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { PhoneNumber } from './phoneNumber';

export class DeliveryGuy {
    id?: string;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    image: string;
    deliveryID?: string;
    // notification: Notification[]; // sub collection
    // totalFess: number; // get it from deliveries
}
