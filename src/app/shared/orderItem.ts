import { Item } from './item';

export class OrderItem {
    id?: string;
    item: Item;
    quantity: number;
}
