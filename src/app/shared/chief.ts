import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { PhoneNumber } from './phoneNumber';

export class Chief {
    id?: string;
    firstName: string;
    lastName: string;
    image?: string;
    mobile: string;
    email: string;
    restaurantID: string;
}
