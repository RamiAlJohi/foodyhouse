import { Component, OnInit } from '@angular/core';
import { ChiefService } from '../../services/chief.service';
import { AuthService } from '../../services/auth.service';
import { Notification } from '../../shared/notification';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-chief-notification',
  templateUrl: './chief-notification.page.html',
  styleUrls: ['./chief-notification.page.scss'],
})
export class ChiefNotificationPage implements OnInit {

  notifications: Notification[];

  constructor(private chiefService: ChiefService, private authService: AuthService, private restaurantList: RestaurantListService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.presentLoading();
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.phoneNumber)
      .subscribe( chief => {
        this.restaurantList.getRestaurantSnap(chief.restaurantID)
        .subscribe( res => {
          this.chiefService.getNotification(res.id)
          .subscribe(notifications => {this.notifications = notifications; this.loadingController.dismiss(); });
        });
      });
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'انتظر. . .',
      duration: 500
    });
    return await loading.present();
  }
}
