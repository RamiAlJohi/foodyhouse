import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChiefNotificationPage } from './chief-notification.page';

const routes: Routes = [
  {
    path: '',
    component: ChiefNotificationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChiefNotificationPage]
})
export class ChiefNotificationPageModule {}
