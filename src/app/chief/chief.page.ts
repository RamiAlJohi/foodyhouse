import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FcmService } from '../services/fcm.service';
import { ChiefService } from '../services/chief.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-chief',
  templateUrl: './chief.page.html',
  styleUrls: ['./chief.page.scss'],
})
export class ChiefPage implements OnInit {
  public appPages = [
    {
      title: 'القائمة',
      url: '/chief/items',
      icon: 'ios-restaurant'
    },
    {
      title: 'الطلبات القادمة',
      url: '/chief/orders',
      icon: 'ios-cart'
    },
    {
      title: 'الحساب',
      url: '/chief/profile',
      icon: 'ios-person'
    },
    // {
    //   title: 'تنبيهات',
    //   url: './notification',
    //   icon: 'ios-alert'
    // },
    {
      title: 'تغيير الحساب',
      url: '/home',
      icon: 'ios-home'
    },

  ];
  constructor(private authService: AuthService, private router: Router, private fcm: FcmService,
              private chiefService: ChiefService) { }

  ngOnInit() {
  }

  signOut() {
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).pipe(first()).toPromise().then( chief => {
        const restaurantId = chief.restaurantID;
        this.fcm.unsub('restaurant' + restaurantId);
        console.log('restaurant' + restaurantId);
        this.authService.userSignOut().then( () => {
          this.router.navigate(['/home']);
        });
      });
    });
  }

}
