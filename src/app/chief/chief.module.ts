import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChiefPage } from './chief.page';
import { ItemsPage } from './items/items.page';
import { AddMealModalPage } from './items/add-meal-modal/add-meal-modal.page';
import { EditMealModalPage } from './items/edit-meal-modal/edit-meal-modal.page';
import { ProfilePage } from './profile/profile.page';
import { OrdersPage } from './orders/orders.page';
import { Order } from '../shared/order';
import { AddLocationPage } from './profile/add-location/add-location.page';
import { AuthGuard } from '../guards/auth.guard';
import { AgmCoreModule } from '@agm/core';
import { ChiefNotificationPage } from './chief-notification/chief-notification.page';

const routes: Routes = [
  {
    path: '',
    component: ChiefPage,
    children: [
      { path: '', redirectTo: 'items', pathMatch: 'full' },
      { path: 'items', component: ItemsPage, canActivate: [AuthGuard]},
      { path: 'profile', component: ProfilePage, canActivate: [AuthGuard] },
      { path: 'orders', component: OrdersPage, canActivate: [AuthGuard]},
      { path: 'notification', component: ChiefNotificationPage, canActivate: [AuthGuard] },
    ]
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8fwBCtKMdrlBa_mC8dqLQSncyBdpCYy4'
    })
  ],
  declarations: [ChiefPage, AddMealModalPage, EditMealModalPage, ItemsPage, ProfilePage, OrdersPage, AddLocationPage,
    ChiefNotificationPage],
  entryComponents: [AddMealModalPage, EditMealModalPage, AddLocationPage]
})
export class ChiefPageModule {}
