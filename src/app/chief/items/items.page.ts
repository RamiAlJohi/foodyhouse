import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, AlertController, Platform, LoadingController } from '@ionic/angular';
import { ChiefService } from '../../services/chief.service';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { AuthService } from '../../services/auth.service';
import { Item } from '../../shared/item';
import { AddMealModalPage } from './add-meal-modal/add-meal-modal.page';
import { EditMealModalPage } from './edit-meal-modal/edit-meal-modal.page';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { FcmService } from '../../services/fcm.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.page.html',
  styleUrls: ['./items.page.scss'],
})
export class ItemsPage implements OnInit {

  searchVal = '';
  sorts = ['price', 'name'];
  sortVal: string = this.sorts[0];
  items: Item[];
  constructor(public modalController: ModalController,
    private chiefService: ChiefService,
    private authService: AuthService,
    private restaurantList: RestaurantListService,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private location: Location,
    private platform: Platform,
    private router: Router,
    private previousRouteService: PreviousRouteService,
    private fcm: FcmService,
    public loadingController: LoadingController) {
      if (this.previousRouteService.getPreviousUrl() !== '/auth/auth-chief/login' ||
          this.previousRouteService.getPreviousUrl() !== '/auth/auth-chief/register-restaurant') {
        this.platform.backButton.subscribe( () => {
          this.router.navigate([this.previousRouteService.getPreviousUrl()]);
        });
      }
    }

  ngOnInit() {
    this.presentLoading();
    this.authService.getUser().then(user => {
      console.log(user.email);
      this.chiefService.getChief(user.email).subscribe(chief => {
        console.log(chief.restaurantID);
        this.restaurantList.getItemsSnap(chief.restaurantID).subscribe(items => {
          for (let i = 0; i < items.length; i++) {
            console.log(items[i].image);
          }
          this.items = items;
          this.loadingController.dismiss();
        });
      });
    });
    this.fcm.getPremission().subscribe(() => {
      if (this.fcm.token != null) {
        this.authService.getUser().then( user => {
          this.chiefService.getChief(user.email).subscribe( chief => {
            this.fcm.sub('restaurant' + chief.restaurantID);
          });
        });
      }
    });
    this.fcm.listenToMessages().subscribe();
  }

  sortChange(evn) {
    this.sortVal === this.sorts[1] ?
    this.items.sort((x, y) => x.name < y.name ? -1 : 1) : this.items.sort((x, y) => x.price > y.price ? -1 : 1);
  }


  async presentModal() {
    const modal = await this.modalController.create({
      component: AddMealModalPage
    });
    return await modal.present();
  }

  async presentAlertConfirm(dish) {
    const alert = await this.alertCtrl.create({
      header: 'تأكيد!',
      message: '!!!هل انت متاكد انك تريد مسح هذ الطبق',
      buttons: [
        {
          text: 'لا',
          role: 'cancel',
          cssClass: 'danger',
        }, {
          text: 'نعم',
          handler: () => { this.deleteDish(dish); }
        }
      ]
    });

    await alert.present();
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss().then(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  async editDish(item) {
    const itemID = item.id;
    const modal = await this.modalController.create({
      component: EditMealModalPage,
      componentProps: { id: itemID}
    });
    return await modal.present();
  }

  deleteDish(item) {

    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        this.chiefService.deleteItem(chief.restaurantID, item.id);
      });
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'انتظر. . .',
      duration: 500
    });
    return await loading.present();
  }
}
