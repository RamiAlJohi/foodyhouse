import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMealModalPage } from './edit-meal-modal.page';

describe('EditMealModalPage', () => {
  let component: EditMealModalPage;
  let fixture: ComponentFixture<EditMealModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMealModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMealModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
