import { OnInit, Component } from '@angular/core';
import { Item, CATEGORIES, CATEGORIESTEXT } from '../../../shared/item';
import { FormGroup, FormControl } from '@angular/forms';
import { ModalController, LoadingController, ToastController, NavParams, Platform } from '@ionic/angular';
import { ChiefService } from '../../../services/chief.service';
import { AuthService } from '../../../services/auth.service';
import { RestaurantListService } from '../../../services/restaurant-list.service';
import { ImageUploadService } from '../../../services/image-upload.service';
import { Camera } from '@ionic-native/camera/ngx';
import { LoadingService } from '../../../services/loading.service';

@Component({
  selector: 'app-edit-meal-modal',
  templateUrl: './edit-meal-modal.page.html',
  styleUrls: ['./edit-meal-modal.page.scss']
})

export class EditMealModalPage implements OnInit {

  categories: string[] = CATEGORIES;
  categoriesText: string[] = CATEGORIESTEXT;

  editMealForm = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    price: new FormControl(0),
    category: new FormControl('')
  });

  item: Item;

  constructor(private modalController: ModalController,
    private loadingService: LoadingService,
    private toastCtrl: ToastController,
    private navParams: NavParams,
    private camera: Camera,
    private chiefService: ChiefService,
    private restaurantList: RestaurantListService,
    private authService: AuthService,
    private imageUploadService: ImageUploadService,
    private platform: Platform) {
      this.platform.backButton.subscribe( () => {
        this.modalController.dismiss();
      });
    }

  ngOnInit() {
    this.item = {name: '', image: '', description: '', price: 0, category: 'food'};
    this.getItem();
  }

  getItem() {
    const itemID = this.navParams.get('id');
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        this.chiefService.getItem(chief.restaurantID, itemID).subscribe(item => {
          this.item = item.payload.data() as Item;
          console.log(item.payload.data());
        });
      });
    });
  }

  getImage() {
    this.imageUploadService.getImage();
  }

  async uploadFile(Id) {
    return await this.imageUploadService.uploadFile(Id);
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss().then(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  submitForm() {
    // const editMeal = this.editMealForm.value;
    // for (const meal in editMeal) {
    //   if (editMeal.hasOwnProperty(meal) ) {
    //     if (editMeal[meal] === '') {
    //       delete editMeal[meal];
    //     }
    //   }
    // }
    const itemId = this.navParams.get('id');
    const editItem = {};
    for (const prop in this.editMealForm.value) {
      if (this.editMealForm.value.hasOwnProperty(prop)) {
        if (this.editMealForm.value[prop] !== '' && this.editMealForm.value[prop] !== 0) {
          editItem[prop] = this.editMealForm.value[prop];
        }
      }
    }

    // console.log(dishId);
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        this.restaurantList.getRestaurantSnap(chief.restaurantID).subscribe( rest => {
          console.log(editItem);
          this.chiefService.updateItem(editItem, chief.restaurantID, itemId);
          if (this.imageUploadService.imageURI !== '' && this.imageUploadService.imageURI !== null &&
              this.imageUploadService.imageURI !== undefined) {
            const path = this.imageUploadService.uploadFile(chief.restaurantID + '/' + itemId);
            path.then(imagePath => {
              imagePath.ref.getDownloadURL().then( downloadUrl => {
                this.chiefService.updateItem({image: downloadUrl}, chief.restaurantID, itemId);
                this.modalController.dismiss();
              });
            });
          } else {
            this.modalController.dismiss();
          }
          // this.modalController.dismiss();
        });
      });
    });
    // this.dishService.editDish(dishId , editMeal);

  }

  close() {
    this.modalController.dismiss();
  }


}
