import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMealModalPage } from './add-meal-modal.page';

describe('AddMealModalPage', () => {
  let component: AddMealModalPage;
  let fixture: ComponentFixture<AddMealModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMealModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMealModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
