import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ModalController, LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { ChiefService } from '../../../services/chief.service';
import { ImageUploadService } from '../../../services/image-upload.service';
import { CATEGORIES, CATEGORIESTEXT } from '../../../shared/item';
import { AuthService } from '../../../services/auth.service';
import { FcmService } from '../../../services/fcm.service';

@Component({
  selector: 'app-add-meal-modal',
  templateUrl: './add-meal-modal.page.html',
  styleUrls: ['./add-meal-modal.page.scss'],
})
export class AddMealModalPage implements OnInit {

  categories: string[] = CATEGORIES;
  categoriesText: string[] = CATEGORIESTEXT;
  addMealForm = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    price: new FormControl(),
    category: new FormControl('')
  });
  constructor(public modalController: ModalController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    private authService: AuthService,
    private chiefService: ChiefService,
    public imageUploadService: ImageUploadService,
    private platform: Platform) {
      this.platform.backButton.subscribe( () => {
        this.modalController.dismiss();
      });
    }

  ngOnInit() {

  }


  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss().then(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  getImage() {
    this.imageUploadService.getImage();
  }

  async uploadFile(Id) {
    return await this.imageUploadService.uploadFile(Id);
  }

  async submitForm() {
    const newMeal = this.addMealForm.value;
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe(chief => {
        const itemRef = this.chiefService.addItem(newMeal, chief.restaurantID);
        itemRef.then(item => {
          if (this.imageUploadService.imageURI !== '' && this.imageUploadService.imageURI !== null &&
          this.imageUploadService.imageURI !== undefined) {
            const path = this.uploadFile(chief.restaurantID + '/' + item.id);
            path.then(imagePath => {
              imagePath.ref.getDownloadURL().then( downloadUrl => {
                this.chiefService.updateItem({image: downloadUrl}, chief.restaurantID, item.id);
                this.modalController.dismiss();
              });
            });
          } else {
            this.chiefService.updateItem({image: this.imageUploadService.imageFileName}, chief.restaurantID, item.id);
            this.modalController.dismiss();
          }
        });
      });
    });
  }

  close() {
    this.modalController.dismiss();
  }

}
