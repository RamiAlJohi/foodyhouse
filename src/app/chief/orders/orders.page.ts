import { Component, OnInit } from '@angular/core';
import { ORDERSTATUS, Order } from '../../shared/order';
import { ChiefService } from '../../services/chief.service';
import { OrderInfo } from '../../shared/orderInfo';
import { OrderItem } from '../../shared/orderItem';
import { AuthService } from '../../services/auth.service';
import { RestaurantService } from '../../services/restaurant.service';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { PreviousRouteService } from '../../services/previous-route.service';
import { Platform, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  status = ORDERSTATUS;
  tabType: string;

  minimumDileveryPrice = 15;

  orders: Order[];
  constructor(private chiefService: ChiefService, private authService: AuthService, private restaurantService: RestaurantService,
              private restaurantList: RestaurantListService, private platform: Platform, private router: Router,
              private previousRouteService: PreviousRouteService, public loadingService: LoadingService) {
                this.platform.backButton.subscribe( () => {
                  this.router.navigate([this.previousRouteService.getPreviousUrl()]);
                });
              }

  ngOnInit() {
    this.loadingService.present(5000);
    this.getOrders();
  }

  getOrders() {
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        this.restaurantList.getRestaurantSnap(chief.restaurantID)
        .subscribe(restaurant => {
          this.chiefService.getOrders(chief.restaurantID).subscribe( orders => {
            this.orders = orders;
            for (const order of this.orders) {
              // console.log(order.);
              const orderInfo: OrderInfo = {
                orderID: order.id,
                customerName: '',
                cutsomerPhoneNumber: '',
                orderItems: [],
                totalPrice: 0
              };
              this.chiefService.getCustomer(order.customerID).subscribe( customer => {
                orderInfo.customerName = customer.firstName + ' ' + customer.lastName;
                orderInfo.cutsomerPhoneNumber = customer.id;
                this.chiefService.getOrderItems(orderInfo.orderID).subscribe(orderItems => {
                  orderInfo.orderItems = orderItems;
                  orderInfo.totalPrice = this.calculateTotal(orderItems);
                  order.orderInfo = orderInfo;
                  orderInfo.distanceToCustomer =
                    this.calculateDistince(restaurant.location.lat, restaurant.location.lng, customer.location.lat, customer.location.lng);
                  orderInfo.deliveryPrice = this.minimumDileveryPrice + orderInfo.distanceToCustomer * 2 ;
                  this.loadingService.dismiss();
                });
              });
            }
          });
        });
      });
    });

  }

  calculateTotal(orderItems: OrderItem[]): number {
    let total = 0;
    for (const i of orderItems) {
      total += (i.quantity * i.item.price);
    }
    return total;
  }

  calculateDistince(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = R * c;
    return Math.round(d);
  }

  changeOrderState(order: Order) {
    let newState = '';
    for (let i = 0; i < this.status.length; i++) {
      if (order.orderStatus === this.status[i] && this.status.length - 1) {
        // order.orderStatus = this.status[i + 1];
        newState = this.status[i + 1];
      }
    }
    console.log(newState);
    this.chiefService.updateOrder(order.id, {orderStatus: newState});
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 3000
  //   });
  //   return await loading.present();
  // }
}
