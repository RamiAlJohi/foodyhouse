import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ToastController, NavController, ModalController, Platform, LoadingController } from '@ionic/angular';
import { Camera } from '@ionic-native/camera/ngx';
import { ImageUploadService } from '../../services/image-upload.service';
import { ChiefService } from '../../services/chief.service';
import { AuthService } from '../../services/auth.service';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { Restaurant, restStatus } from '../../shared/restaurant';
import { RestaurantService } from '../../services/restaurant.service';
import { Time } from '@angular/common';
import { Review } from '../../shared/review';
import { AddLocationPage } from './add-location/add-location.page';
import { PreviousRouteService } from '../../services/previous-route.service';
import { Router } from '@angular/router';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  restaurant: Restaurant;
  workingHoursForm = new FormGroup({
    sunday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
    monday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
    tuesday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
    wensday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
    thursday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
    friday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
    saturday: new FormGroup({
      isOpen: new FormControl(''),
      from: new FormControl(''),
      to: new FormControl('')
    }),
  });

  isEditTimeTable = false;
  isEditPrices = false;
  reviews: Review[];
  status: string[] = restStatus;
  statusText: string[] = ['مفتوح', 'مشغول', 'مغلق'];
  state: string;
  constructor(public modalController: ModalController,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    private camera: Camera,
    private authService: AuthService,
    private chiefService: ChiefService,
    private restaurantList: RestaurantListService,
    private restaurantService: RestaurantService,
    private imageUploadService: ImageUploadService,
    private previousRouteService: PreviousRouteService,
    private platform: Platform,
    private router: Router,
    public loadingService: LoadingService) {
      this.platform.backButton.subscribe( () => {
        this.router.navigate([this.previousRouteService.getPreviousUrl()]);
      });
    }

  ngOnInit() {
    this.loadingService.present(5000);
    this.getRestaurantInfo();
  }

  getRestaurantInfo() {
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        this.restaurantList.getRestaurantSnap(chief.restaurantID).subscribe( res => {
          this.restaurant = res as Restaurant;
          this.loadingService.dismiss();
          // console.log(this.restaurant.workingHours.saturday.from);
        });
        this.restaurantList.getCommentsVal(chief.restaurantID).subscribe( reviews => {
          this.reviews = reviews;
          this.restaurant.rate = this.calculateRating(reviews);
        });
      });
    });
  }
  // this to combine the object time into a string
  timeToString(time: Time): string {
    console.log(time);
    // const time: Time = {hours: 1, minutes: 15};
    return time.hours.toString() + ':' + time.minutes.toString();
  }

  submitEditWorkingHours() {
    for (const prop in this.workingHoursForm.value) {
      if (this.workingHoursForm.value.hasOwnProperty(prop) ) {
        for (const prop2 in this.workingHoursForm.value[prop]) {
          if (this.workingHoursForm.value[prop].hasOwnProperty(prop2)) {
            if (this.workingHoursForm.value[prop][prop2].hasOwnProperty('hour')) {
              const timeConverter = this.workingHoursForm.value[prop][prop2]['hour'].text + ':' +
                                    this.workingHoursForm.value[prop][prop2]['minute'].text;
              this.workingHoursForm.get(prop).get(prop2).setValue(timeConverter);
            }
          }
        }
      }
    }
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {

        this.restaurantService.updateRestaurant({workingHours: this.workingHoursForm.value}, chief.restaurantID);
      });
    });

    this.isEditTimeTable = false;
  }

  editProfileInfo() {
    this.isEditTimeTable = true;
  }

  async setLocation() {
    const modal = await this.modalController.create({
      component: AddLocationPage
    });
    return await modal.present();
  }

  toggle(input, icon) {
    if (input.disabled) { // edit
      input.disabled = false;
      icon.name = 'save';
    } else { // save
      input.disabled = true;
      icon.name = 'create';
      this.authService.getUser().then( user => {
        this.chiefService.getChief(user.email).subscribe( chief => {
          const inputName = input.name;
          const newInfo = {};
          newInfo[inputName] = input.value;
          this.restaurantService.updateRestaurant(newInfo, chief.restaurantID);
        });
      });
      // function to edit database

    }
  }

  calculateRating(reviews: Review[]): number {
    let total = 0;
    for (const i of reviews) {
      total += i.rate;
    }
    return (reviews.length !== 0) ?  total / reviews.length : 0;
  }

  async getImage() {
    await this.imageUploadService.getImage();
    this.upload();
  }

  changeRestaurantStatus(status) {
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        console.log(status.value);
        this.restaurantService.updateRestaurant({restaurantStatus: status.value}, chief.restaurantID);
      });
    });
  }

  upload() {
    this.authService.getUser().then( user => {
      this.chiefService.getChief(user.email).subscribe( chief => {
        this.loadingService.present(5000);
        const path = this.imageUploadService.uploadFile(chief.restaurantID);
        path.then(imagePath => {
          imagePath.ref.getDownloadURL().then( downloadUrl => {
            this.loadingService.dismiss();
            // this.chiefService.updateItem({image: downloadUrl}, chief.restaurantID, item.id);
            this.restaurantService.updateRestaurant({image: downloadUrl}, chief.restaurantID);
          });
        });
      });
    });
    //  await this.imageUploadService.uploadFile(Id);
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss().then(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  cancelEditTimeTable() {
    this.isEditTimeTable = false;
    console.log(this.isEditTimeTable);
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 500
  //   });
  //   return await loading.present();
  // }
}
