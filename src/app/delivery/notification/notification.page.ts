import { Component, OnInit } from '@angular/core';

import { Notification } from '../../shared/notification';
// import { DELIVERYGUYS } from '../../shared/deliveryguys';

import { FcmService } from '../../services/fcm.service';
import { DeliveryService } from '../../services/delivery.service';
import { AuthService } from '../../services/auth.service';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  notifications: Notification[]; // = DELIVERYGUYS[0].notification;

  constructor(public fcm: FcmService, private authService: AuthService, private deliveryService: DeliveryService,
              private platform: Platform, private router: Router, private previousRouteService: PreviousRouteService) {
    this.fcm.listenToMessages();
    this.platform.backButton.subscribe( () => {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    });
  }

  ngOnInit() {
  }

  // getPermission() {
  //   this.fcm.getPermission().subscribe();
  // }
  getPremission() {
    this.fcm.getPremission().subscribe();
  }
  randomDiscount() {
    const random = Math.round(Math.random() * 100);

    const headline = `New discount for ${random}% off!!!`;

    this.fcm.updateAt('discounts', { headline });
  }
  sub() {
    this.authService.getUser().then( user => {
      this.deliveryService.getDeliveryTest(user.phoneNumber).subscribe( deliveryGuy => {
        const deliveryGuyPath = deliveryGuy.payload.ref.path.slice(deliveryGuy.payload.ref.path.indexOf('/') + 2);
         this.fcm.sub('deliveryGuys' + deliveryGuyPath);
      });
      // const phoneNum = user.phoneNumber.slice(1);
    });
  }

  unsub() {
    this.authService.getUser().then( user => {
      this.deliveryService.getDeliveryTest(user.phoneNumber).subscribe( deliveryGuy => {
        const deliveryGuyPath = deliveryGuy.payload.ref.path.slice(deliveryGuy.payload.ref.path.indexOf('/') + 2);
        this.fcm.unsub(deliveryGuy);
      });
    });
  }
}
