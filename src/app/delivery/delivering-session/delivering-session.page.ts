import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { ModalController, Platform, LoadingController } from '@ionic/angular';
import { Marker } from '../../shared/marker';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Delivery, DELIVERYSTATUS } from '../../shared/delivery';

import { GoogleMapsAPIWrapper, AgmMap, LatLngBounds, LatLngBoundsLiteral} from '@agm/core'; // test
import { Order, ORDERSTATUS } from '../../shared/order';
import { DeliveryService } from '../../services/delivery.service';
import { AuthService } from '../../services/auth.service';
import { OrderInfo } from '../../shared/orderInfo';
import { OrderItem } from '../../shared/orderItem';

import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { LoadingService } from '../../services/loading.service';

declare var google: any;

@Component({
  selector: 'app-delivering-session',
  templateUrl: './delivering-session.page.html',
  styleUrls: ['./delivering-session.page.scss'],
})
export class DeliveringSessionPage implements OnInit {

  // @Input() order: Order;
  delivery: Delivery;
  order: Order;
  minimumDileveryPrice = 15;

  statusText: string[] = ['تم الوصول الى لمطعم', 'تم الاسلام', 'تم الوصل الى الزبون', 'تم التسليم', 'انتهت المهمة'];
  status: number;


  @ViewChild('AgmMap') agmMap: AgmMap; // test

  zoom = 10;

  lat = 21.54238;
  lng = 21.54238;

  markerDriver: Marker = {
    lat: null,
    lng: null,
    label: '',
    draggable: false
  };

  markerHome: Marker = {
    lat: null,
    lng: null,
    label: '',
    draggable: false
  };

  markerShop: Marker = {
    lat: null,
    lng: null,
    label: '',
    draggable: false
  };

  constructor(private modalController: ModalController, private geolocation: Geolocation, private deliveryService: DeliveryService,
              private authService: AuthService, private launchNavigator: LaunchNavigator, private platform: Platform,
              private router: Router, private previousRouteService: PreviousRouteService, public loadingService: LoadingService) {
                this.platform.backButton.subscribe( () => {
                  this.router.navigate([this.previousRouteService.getPreviousUrl()]);
                });
              }

  ngOnInit() {
    this.loadingService.present(5000);
    this.getLatLon()
    .then((resp) => {
      this.markerDriver.lat = resp.coords.latitude;
      this.markerDriver.lng = resp.coords.longitude;
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      console.log('latitude = ' + this.markerDriver.lat + ', longitude = ' + this.markerDriver.lng);
      this.authService.getUser()
      .then(user => {
        this.deliveryService.getDeliveryGuy(user.email)
        .subscribe(deliveryGuy => {
          if (deliveryGuy.deliveryID !== undefined && deliveryGuy.deliveryID !== null) {
          this.deliveryService.getDelivery(deliveryGuy.deliveryID)
          .subscribe(delivery => {
            console.log(delivery);
            if (delivery.deliveryStatus === DELIVERYSTATUS[0]) {
              this.status = 0;
            } else if (delivery.deliveryStatus === DELIVERYSTATUS[1]) {
              this.status = 1;
            } else if (delivery.deliveryStatus === DELIVERYSTATUS[2]) {
              this.status = 2;
            } else if (delivery.deliveryStatus === DELIVERYSTATUS[3]) {
              this.status = 3;
            } else if (delivery.deliveryStatus === DELIVERYSTATUS[4]) {
              this.status = 4;
            }
            this.delivery = delivery;
            this.deliveryService.getOrderSnap(this.delivery.orderID)
            .subscribe(order => {
              const orderInfo: OrderInfo = {
                orderID: order.id,
                restaurantName: '',
                orderItems: [],
                totalPrice: 0
              };

              this.deliveryService.getRestaurantSnap(order.restaurantID)
              .subscribe(restaurant => {
                orderInfo.restaurantName = restaurant.name;
                orderInfo.restaurantLocation = restaurant.location;
                orderInfo.restImage = restaurant.image;
                this.deliveryService.getOrderItems(order.id)
                .subscribe(orderItems => {
                  orderInfo.orderItems = orderItems;
                  orderInfo.totalPrice = this.calculateTotal(orderItems);

                  this.deliveryService.getCustomer(order.customerID)
                  .subscribe(customer => {
                    orderInfo.customerLocation = customer.location;
                    orderInfo.distanceToRestaurant =
                      this.calculateDistince(this.markerDriver.lat, this.markerDriver.lng,
                                              restaurant.location.lat, restaurant.location.lng);
                    orderInfo.distanceToCustomer =
                      this.calculateDistince(restaurant.location.lat, restaurant.location.lng,
                                              customer.location.lat, customer.location.lng);
                    orderInfo.deliveryPrice = this.minimumDileveryPrice + orderInfo.distanceToCustomer * 2 ;
                    console.log(orderInfo);
                    order.orderInfo = orderInfo;
                    this.order = order;
                    this.markersSeting();
                    this.loadingService.dismiss();
                  });
                });
              });
            });
          });
        }
        });
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  // onMapReady() {
  //   console.log(this.agmMap);
  //   this.agmMap.mapReady.subscribe(map => {
  //     const bounds: LatLngBounds = new google.maps.LatLngBounds();
  //     for (const mm of this.markers) {
  //       bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
  //     }
  //     map.fitBounds(bounds);
  //   });
  // }

  onMapReady(map: AgmMap) { // test
    const markers = [this.markerDriver, this.markerHome, this.markerShop];
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    for (const mm of markers) {
      bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
    }
    map.fitBounds = bounds;
  }

  mapIdle() { // test
    console.log(this.zoom);
  }

  markersSeting() {
    this.markerShop.lat = this.order.orderInfo.restaurantLocation.lat;
    this.markerShop.lng = this.order.orderInfo.restaurantLocation.lng;
    this.markerHome.lat = this.order.orderInfo.customerLocation.lat;
    this.markerHome.lng = this.order.orderInfo.customerLocation.lng;
  }

  getLatLon() {
    return this.geolocation.getCurrentPosition();
  }

  calculateDistince(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = R * c;
    return Math.round(d);
  }

  mapClicked($event) {
    this.markerDriver.lat = $event.coords.lat;
    this.markerDriver.lng = $event.coords.lng;
  }

  calculateTotal(orderItems: OrderItem[]): number {
    let total = 0;
    for (const i of orderItems) {
      total += (i.quantity * i.item.price);
    }
    return total;
  }

  goToRestaurant() {
    this.launchNavigator.navigate([this.markerShop.lat, this.markerShop.lng], {
      start: this.markerDriver.lat + ', ' + this.markerDriver.lng
    });
  }

  goToCustomer() {
    this.launchNavigator.navigate([this.markerHome.lat, this.markerHome.lng], {
      start: this.markerDriver.lat + ', ' + this.markerDriver.lng
    });
  }

  receiving() {
    if (this.status === 0) {
      this.deliveryService.updateOrderStatus(this.order.id, ORDERSTATUS[4])
      .then(done => this.deliveryService.updateDeliveryStatus(this.delivery.id, DELIVERYSTATUS[1]));
    } else if (this.status === 1) {
      this.deliveryService.updateDeliveryStatus(this.delivery.id, DELIVERYSTATUS[2]);
      this.deliveryService.updateOrderStatus(this.order.id, ORDERSTATUS[5]);
    } else if (this.status === 2) {
      this.deliveryService.updateDeliveryStatus(this.delivery.id, DELIVERYSTATUS[3])
      .then(done => {
        this.deliveryService.updateOrderStatus(this.order.id, ORDERSTATUS[6]);
      });
    } else if (this.status === 3) {
      this.deliveryService.updateDeliveryStatus(this.delivery.id, DELIVERYSTATUS[4])
      .then(done => this.authService.getUser()
        .then(user => {
          this.deliveryService.updateOrderStatus(this.order.id, ORDERSTATUS[7])
          .then(orderRecived => this.deliveryService.freeDeliveryGuySession(user.email));
        })
      );
    }
    console.log(this.delivery.deliveryStatus);
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000
  //   });
  //   return await loading.present();
  // }
}

