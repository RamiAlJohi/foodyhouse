import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DeliveringSessionPage } from './delivering-session.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveringSessionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [DeliveringSessionPage]
})
export class DeliveringSessionPageModule {}
