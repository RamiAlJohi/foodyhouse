import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveringSessionPage } from './delivering-session.page';

describe('DeliveringSessionPage', () => {
  let component: DeliveringSessionPage;
  let fixture: ComponentFixture<DeliveringSessionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveringSessionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveringSessionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
