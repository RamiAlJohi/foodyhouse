import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../../services/delivery.service';
import { AuthService } from '../../services/auth.service';
import { Notification } from '../../shared/notification';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-delivery-notification',
  templateUrl: './delivery-notification.page.html',
  styleUrls: ['./delivery-notification.page.scss'],
})
export class DeliveryNotificationPage implements OnInit {

  notifications: Notification[];

  constructor(private deliveryService: DeliveryService, private authService: AuthService, public loadingController: LoadingController) { }

  ngOnInit() {
    this.presentLoading();
    this.authService.getUser()
    .then(user => {
      this.deliveryService.getNotification(user.phoneNumber)
      .subscribe(notifications => {this.notifications = notifications; this.loadingController.dismiss(); });
    });
  }

  addNotify() {
    this.authService.getUser()
    .then(user => {
      const notify: Notification = {
        headline: 'تجربة',
        date: Date.now().toString()
      };
      this.deliveryService.addNotify(user.phoneNumber, notify);
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'انتظر. . .',
      duration: 500
    });
    return await loading.present();
  }

}
