import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DeliveryPage } from './delivery.page';
import { OrderListPage } from './order-list/order-list.page';
import { NotificationPage } from './notification/notification.page';
import { DeliveryProfilePage } from './delivery-profile/delivery-profile.page';
import { OrderDetailsPage } from './order-details/order-details.page';
import { DeliveringSessionPage } from './delivering-session/delivering-session.page';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { AgmCoreModule } from '@agm/core';
import { DeliveryNotificationPage } from './delivery-notification/delivery-notification.page';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: DeliveryPage,
    children: [
      { path: '', redirectTo: 'orderList' },
      { path: 'orderList', component: OrderListPage, canActivate: [AuthGuard]},
      { path: 'profile', component: DeliveryProfilePage, canActivate: [AuthGuard] },
      // { path: 'notification', component: DeliveryNotificationPage },
      { path: 'deliveringSession', component: DeliveringSessionPage, canActivate: [AuthGuard] }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8fwBCtKMdrlBa_mC8dqLQSncyBdpCYy4'
    })

  ],
  providers: [LaunchNavigator],
  declarations: [DeliveryPage, OrderListPage, DeliveryProfilePage, NotificationPage, DeliveringSessionPage, DeliveryNotificationPage,
                    OrderDetailsPage],
  entryComponents: [OrderDetailsPage]
})
export class DeliveryPageModule {}
