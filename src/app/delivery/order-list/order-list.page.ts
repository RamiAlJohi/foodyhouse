import { Component, OnInit, ViewChild } from '@angular/core';

import { Delivery } from '../../shared/delivery';
// import { DELIVERIES } from '../../shared/deliveries';

import { ModalController, Platform, LoadingController, InfiniteScroll } from '@ionic/angular';
import { OrderDetailsPage } from '../order-details/order-details.page';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Location } from '../../shared/location';
import { DeliveryService } from '../../services/delivery.service';
import { Order } from '../../shared/order';
import { OrderInfo } from '../../shared/orderInfo';
import { OrderItem } from '../../shared/orderItem';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.page.html',
  styleUrls: ['./order-list.page.scss'],
})
export class OrderListPage implements OnInit {

  deliveries: Delivery[]; // = DELIVERIES;
  orders: Order[];
  currLocation: LatLng = { lat: null, lng: null};
  minimumDileveryPrice = 15;

  rate = 3;

  totalNumOfOrders = 0;
  searchVal = '';
  sorts = ['shopDistence', 'customerDistence'];
  sortVal: string = this.sorts[0];

  dataLimit = 5;
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  constructor(public modalController: ModalController, private geolocation: Geolocation, private deliveryService: DeliveryService,
              private platform: Platform, private router: Router, private previousRouteService: PreviousRouteService,
              public loadingService: LoadingService) {
    if ( this.previousRouteService.getPreviousUrl() !== 'auth/auth-delivery/login' ||
         this.previousRouteService.getPreviousUrl() !== 'auth/auth-delivery/register-delivery') {
          this.platform.backButton.subscribe( () => {
            this.router.navigate([this.previousRouteService.getPreviousUrl()]);
          });
    }

  }

  ngOnInit() {
    this.loadingService.present(5000);
    this.deliveryService.getOrderSize().subscribe( orders => {
      this.totalNumOfOrders = orders.length;
      // console.log(this.totalNumOfOrders);
    });
    this.getLatLon()
    .then(location =>
      this.deliveryService.getCompletedOrders(this.dataLimit)
      .subscribe(orders => {
        console.log(orders);
        this.orders = orders;
        for (const i of this.orders) {
          const orderInfo: OrderInfo = {
            orderID: i.id,
            restaurantName: '',
            orderItems: [],
            totalPrice: 0
          };

          this.deliveryService.getRestaurantSnap(i.restaurantID)
          .subscribe(restaurant => {
            orderInfo.restaurantName = restaurant.name;
            orderInfo.restaurantLocation = restaurant.location;
            orderInfo.restImage = restaurant.image;
            this.deliveryService.getOrderItems(i.id)
            .subscribe(orderItems => {
              orderInfo.orderItems = orderItems;
              orderInfo.totalPrice = this.calculateTotal(orderItems);

              this.deliveryService.getCustomer(i.customerID)
              .subscribe(customer => {
                orderInfo.customerLocation = customer.location;
                orderInfo.distanceToRestaurant =
                  this.calculateDistince(this.currLocation.lat, this.currLocation.lng, restaurant.location.lat, restaurant.location.lng);
                orderInfo.distanceToCustomer =
                  this.calculateDistince(restaurant.location.lat, restaurant.location.lng, customer.location.lat, customer.location.lng);
                orderInfo.deliveryPrice = this.minimumDileveryPrice + orderInfo.distanceToCustomer * 2 ;
                console.log(orderInfo);
                i.orderInfo = orderInfo;
                this.loadingService.dismiss();
              });
            });
          });
        }
      })
    );
  }

  async orderDetailsModal(order: Order) {
    if (order.orderInfo) {
      const modal = await this.modalController.create({
        component: OrderDetailsPage,
        componentProps: { order: order, currLocation: this.currLocation }
      });
      return await modal.present();
    } else {return 0; }
  }

  getLatLon() {
    return this.geolocation.getCurrentPosition().then((resp) => {
      // this.currLocation.name = 'جدة';
      this.currLocation.lat = resp.coords.latitude;
      this.currLocation.lng = resp.coords.longitude;
      console.log('latitude = ' + this.currLocation.lat + ', longitude = ' + this.currLocation.lng);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  calculateDistince(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = R * c;
    return Math.round(d);
  }

  sortChange() {
    this.sortVal === this.sorts[0] ?
    this.orders.sort((x, y) => x.orderInfo.distanceToRestaurant <  y.orderInfo.distanceToRestaurant ? -1 : 1) :
    this.orders.sort((x, y) => x.orderInfo.distanceToCustomer <  y.orderInfo.distanceToCustomer ? -1 : 1);

    // this.sortVal === this.sorts[0] ?
    // this.orders.sort((x, y) =>
    // Number(document.getElementsByClassName(x.id)[0]
    // .getElementsByClassName('restaurantDistance')[0].innerHTML.toString()) <
    // Number(document.getElementsByClassName(y.id)[0]
    // .getElementsByClassName('restaurantDistance')[0].innerHTML.toString()) ? -1 : 1) :

    // this.orders.sort((x, y) =>
    // Number(document.getElementsByClassName(x.id)[0]
    // .getElementsByClassName('customerDistance')[0].innerHTML.toString()) <
    // Number(document.getElementsByClassName(y.id)[0]
    // .getElementsByClassName('customerDistance')[0].innerHTML.toString()) ? -1 : 1);
  }

  calculateTotal(orderItems: OrderItem[]): number {
    let total = 0;
    for (const i of orderItems) {
      total += (i.quantity * i.item.price);
    }
    return total;
  }

  loadData(event) {
    this.dataLimit += 5;
    console.log(this.dataLimit);
    setTimeout( () => {
      this.getLatLon()
      .then(location =>
        this.deliveryService.getCompletedOrders(this.dataLimit)
        .subscribe(orders => {
          console.log(orders);
          // this.orders = orders;
          for (let i = this.orders.length; i < orders.length; i++) {
            this.orders.push(orders[i]);
          }
          for (const i of this.orders) {
            const orderInfo: OrderInfo = {
              orderID: i.id,
              restaurantName: '',
              orderItems: [],
              totalPrice: 0
            };
            this.deliveryService.getRestaurantSnap(i.restaurantID)
            .subscribe(restaurant => {
              orderInfo.restaurantName = restaurant.name;
              orderInfo.restaurantLocation = restaurant.location;
              orderInfo.restImage = restaurant.image;
              this.deliveryService.getOrderItems(i.id)
              .subscribe(orderItems => {
                orderInfo.orderItems = orderItems;
                orderInfo.totalPrice = this.calculateTotal(orderItems);
                this.deliveryService.getCustomer(i.customerID)
                .subscribe(customer => {
                  orderInfo.customerLocation = customer.location;
                  orderInfo.distanceToRestaurant =
                    this.calculateDistince(this.currLocation.lat, this.currLocation.lng, restaurant.location.lat, restaurant.location.lng);
                  orderInfo.distanceToCustomer =
                    this.calculateDistince(restaurant.location.lat, restaurant.location.lng, customer.location.lat, customer.location.lng);
                  orderInfo.deliveryPrice = this.minimumDileveryPrice + orderInfo.distanceToCustomer * 2 ;
                  console.log(orderInfo);
                  i.orderInfo = orderInfo;
                  event.target.complete();
                  if (this.orders.length >= this.totalNumOfOrders) {
                    console.log(this.orders.length);
                    event.target.disabled = true;
                  }
                  // this.loadingService.dismiss();
                });
              });
            });
          }
        })
      );
    }, 1000);

  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 4000
  //   });
  //   return await loading.present();
  // }
}

export interface LatLng {
  lat: number;
  lng: number;
}
