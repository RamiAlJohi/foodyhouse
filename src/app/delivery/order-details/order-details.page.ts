import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { ModalController, Platform, LoadingController } from '@ionic/angular';
import { Marker } from '../../shared/marker';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Delivery, DELIVERYSTATUS } from '../../shared/delivery';

import { GoogleMapsAPIWrapper, AgmMap, LatLngBounds, LatLngBoundsLiteral} from '@agm/core'; // test
import { Order, ORDERSTATUS } from '../../shared/order';
import { DeliveryService } from '../../services/delivery.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

declare var google: any;

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {

  @Input() order: Order;
  @Input() currLocation: any;

  @ViewChild('AgmMap') agmMap: AgmMap; // test

  zoom = 10;
  distence = 13;

  lat = 21.54238;
  lng = 21.54238;

  markerDriver: Marker = {
    lat: null,
    lng: null,
    label: '',
    draggable: false
  };

  markerHome: Marker = {
    lat: null,
    lng: null,
    label: '',
    draggable: false
  };

  markerShop: Marker = {
    lat: null,
    lng: null,
    label: '',
    draggable: false
  };

  disabled = true;

  constructor(private modalController: ModalController, private geolocation: Geolocation, private deliveryService: DeliveryService,
              private authService: AuthService, private router: Router, private platform: Platform) {
                this.platform.backButton.subscribe( () => {
                  this.modalController.dismiss();
                });
              }

  ngOnInit() {
    this.getLatLon();
    this.markersSeting();
    this.authService.getUser()
    .then(user => {
      this.deliveryService.getDeliveryGuy(user.email)
      .subscribe(deliveryGuy => {
        console.log(deliveryGuy);
        if (deliveryGuy.deliveryID === null || deliveryGuy.deliveryID === undefined || deliveryGuy.deliveryID === '') {
          this.disabled = false;
        } else { this.disabled = true; }
      });
    });
  }

  // onMapReady() {
  //   console.log(this.agmMap);
  //   this.agmMap.mapReady.subscribe(map => {
  //     const bounds: LatLngBounds = new google.maps.LatLngBounds();
  //     for (const mm of this.markers) {
  //       bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
  //     }
  //     map.fitBounds(bounds);
  //   });
  // }

  onMapReady(map: AgmMap) { // test
    const markers = [this.markerDriver, this.markerHome, this.markerShop];
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    for (const mm of markers) {
      bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
    }
    map.fitBounds = bounds;
  }

  mapIdle() { // test
    console.log(this.zoom, this.lat, this.lng);
  }

  markersSeting() {
    this.markerDriver.lat = this.currLocation.lat;
    this.markerDriver.lng = this.currLocation.lng;
    this.markerShop.lat = this.order.orderInfo.restaurantLocation.lat;
    this.markerShop.lng = this.order.orderInfo.restaurantLocation.lng;
    this.markerHome.lat = this.order.orderInfo.customerLocation.lat;
    this.markerHome.lng = this.order.orderInfo.customerLocation.lng;
    this.mapSeting();
  }

  mapSeting() {
    this.lat = (this.markerDriver.lat + this.markerShop.lat + this.markerHome.lat) / 3;
    this.lng = (this.markerDriver.lng + this.markerShop.lng + this.markerHome.lng) / 3;
    // this.zooming(); // trying to calculate zoom value
  }

  zooming() {
    const lats = [this.markerDriver.lat, this.markerShop.lat, this.markerHome.lat];
    const lngs = [this.markerDriver.lng, this.markerShop.lng, this.markerHome.lng];

    lats.sort((a, b) => a - b);
    lngs.sort((a, b) => a - b);

    const latDeference = Math.abs(lats[0] - lats[2]);
    const lngDeference = Math.abs(lngs[0] - lngs[2]);
    const maxDeference = Math.max(latDeference, lngDeference);
    console.log('zooming: ', maxDeference);
  }

  getLatLon() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.markerDriver.lat = resp.coords.latitude;
      this.markerDriver.lng = resp.coords.longitude;
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      console.log('latitude = ' + this.markerDriver + ', longitude = ' + this.markerDriver);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  calculateDistince(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = R * c;
    return Math.round(d);
  }

  close() {
    this.modalController.dismiss();
  }

  mapClicked($event) {
    this.markerDriver.lat = $event.coords.lat;
    this.markerDriver.lng = $event.coords.lng;
  }

  confirm() {
    this.authService.getUser()
    .then(user => {
      const delivery: Delivery = {
        orderID: this.order.id,
        deliveryStatus: DELIVERYSTATUS[0],
        deliveryGuyID: user.phoneNumber,
        customerLocation: this.order.orderInfo.customerLocation,
        restaurantLocation: this.order.orderInfo.restaurantLocation,
        deliveryFees: this.order.orderInfo.deliveryPrice
      };
      console.log(delivery);
      this.deliveryService.updateOrderStatus(this.order.id, ORDERSTATUS[3])
      .then(updating => {
        this.deliveryService.addDelivery(delivery)
        .then(newDelivery => {
          this.deliveryService.assignDelivery(user.email, newDelivery.id)
          .then(assigning => {
            this.router.navigate(['/delivery/deliveringSession']);
            this.close();
          });
        });
      });
    });
  }
}
