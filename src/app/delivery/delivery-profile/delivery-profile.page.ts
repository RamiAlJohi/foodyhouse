import { Component, OnInit } from '@angular/core';

import { DeliveryGuy } from '../../shared/deliveryguy';
import { DeliveryService } from '../../services/delivery.service';
import { AuthService } from '../../services/auth.service';
import { Platform, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { ImageUploadService } from '../../services/image-upload.service';
import { first } from 'rxjs/operators';
import { LoadingService } from '../../services/loading.service';
// import { DELIVERYGUYS } from '../../shared/deliveryguys';



@Component({
  selector: 'app-delivery-profile',
  templateUrl: './delivery-profile.page.html',
  styleUrls: ['./delivery-profile.page.scss'],
})
export class DeliveryProfilePage implements OnInit {

  deliveryGuy: DeliveryGuy; // = DELIVERYGUYS[0];
  numberOfDeliveries: number;

  constructor(private deliveryService: DeliveryService, private authService: AuthService, private platform: Platform,
              private router: Router, private previousRouteService: PreviousRouteService, public imageUploadService: ImageUploadService,
              public loadingService: LoadingService) {
    this.platform.backButton.subscribe( () => {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    });
  }

  ngOnInit() {
    console.log('1');
    this.loadingService.present(5000);
    this.authService.getUser()
    .then(user => {
      this.deliveryService.getDeliveryGuy(user.email)
      .subscribe(deliveryGuy => {
        this.deliveryGuy = deliveryGuy;
        this.deliveryService.getDeliveryGuyDeliveries(deliveryGuy.id)
        .subscribe(deliveries => {
          this.numberOfDeliveries = deliveries.length;
          this.loadingService.dismiss();
        });
      });
    });
  }

  toggle(input, icon) {
    if (input.disabled) { // edit
      input.disabled = false;
      icon.name = 'save';
    } else { // save
      input.disabled = true;
      icon.name = 'create';

      if (input.name === 'firstName') {
        this.deliveryService.updateDeliveryGuyFirstName(this.deliveryGuy.id, input.value);
      } else if (input.name === 'lastName') {
        this.deliveryService.updateDeliveryGuyLastName(this.deliveryGuy.id, input.value);
      } else if (input.name === 'mobile') {
        this.deliveryService.updateDeliveryGuyMobile(this.deliveryGuy.id, input.value);
      } else {console.log('WTF...'); }
      // function to edit database
      console.log(input.name, input.value);
    }
  }

  async getImage() {
    await this.imageUploadService.getImage();
      this.upload();
  }

  upload() {
    this.authService.getUser().then( user => {
      this.deliveryService.getDeliveryGuy(user.email).pipe(first()).toPromise().then( deliveryGuy => {
        console.log('wtf' + ' ' + user.email);
        this.loadingService.present(5000);
        const path = this.imageUploadService.uploadFile(user.email);
        path.then(imagePath => {
          imagePath.ref.getDownloadURL().then( downloadUrl => {
            this.loadingService.dismiss();
            // this.chiefService.updateItem({image: downloadUrl}, chief.restaurantID, item.id);
            this.deliveryService.updateDeliveryGuy(user.email, {image: downloadUrl});
          });
        });
      });
    });
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000
  //   });
  //   return await loading.present();
  // }
}
