import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.page.html',
  styleUrls: ['./delivery.page.scss'],
})
export class DeliveryPage implements OnInit {

  public appPages = [
    {
      title: 'طلبات',
      url: './orderList',
      icon: 'ios-restaurant'
    },
    // {
    //   title: 'تنبيهات',
    //   url: './notification',
    //   icon: 'ios-alert'
    // },
    {
      title: 'أنا',
      url: './profile',
      icon: 'ios-person'
    },
    {
      title: 'توصيل',
      url: './deliveringSession',
      icon: 'car'
    },
    {
      title: 'تغيير الحساب',
      url: '/home',
      icon: 'ios-home'
    },
  ];

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signOut() {
    this.authService.userSignOut().then(whatever =>
      this.router.navigate(['/home'])
    );
  }

}
