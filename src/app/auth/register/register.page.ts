import { Component, OnInit } from '@angular/core';
import { ToastController, Platform } from '@ionic/angular';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registrationForm = new FormGroup({
    email: new FormControl(''),
    passwords: new FormGroup({
      password: new FormControl(''),
      confirmPass: new FormControl('')
    }, {validators: this.checkPasswords})
  });
  constructor(private toastCtrl: ToastController, private authService: AuthService, private router: Router,
              private location: Location, private platform: Platform) { 
    this.platform.backButton.subscribe( () => {
      this.location.back();
    });
  }

  ngOnInit() {
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPass').value;

    return pass === confirmPass ||  pass === '' || confirmPass === '' ? null : { notSame: true };
  }

  register() {
    const email = this.registrationForm.value.email;
    const password = this.registrationForm.value.passwords.password;
    this.authService.register(email, password).then( user => {
      console.log('user has successfuly register.');
      this.location.back();
    })
    .catch(err => this.authService.registerErrors(err));
  }

  loginPage() {
    this.router.navigate(['']);
  }

  goBack() {
    this.location.back();
  }

}
