import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterChiefFormPage } from './register-chief-form.page';

describe('RegisterChiefFormPage', () => {
  let component: RegisterChiefFormPage;
  let fixture: ComponentFixture<RegisterChiefFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterChiefFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterChiefFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
