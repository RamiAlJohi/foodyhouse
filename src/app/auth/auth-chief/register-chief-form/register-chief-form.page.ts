import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ChiefService } from '../../../services/chief.service';
import { AuthService } from '../../../services/auth.service';
import { Chief } from '../../../shared/chief';
import { ImageUploadService } from '../../../services/image-upload.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-register-chief-form',
  templateUrl: './register-chief-form.page.html',
  styleUrls: ['./register-chief-form.page.scss'],
})
export class RegisterChiefFormPage implements OnInit {

  registrationForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    mobile: new FormControl(''),
  });

  constructor(private authService: AuthService, private router: Router,
              private chiefService: ChiefService, private imageUploadService: ImageUploadService,
              private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  getImage() {
    this.imageUploadService.getImage();
  }

  async uploadFile(Id) {
    return await this.imageUploadService.uploadFile(Id);
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPass').value;

    return pass === confirmPass ||  pass === '' || confirmPass === '' ? null : { notSame: true };
  }


  registerChief() {
    const chief: Chief = this.registrationForm.value;
    // const password = this.registrationForm.value.passwords.password;
    this.authService.getUser().then( user => {
      console.log(user);
      chief.email = user.email;
      const chiefPath = this.chiefService.newChief(chief, user.email);
      this.chiefService.userEmail = user.email;
      // chiefPath.valueChanges().pipe(first()).toPromise().then( chiefInfo => {
        if (this.imageUploadService.imageURI !== '' && this.imageUploadService.imageURI !== null &&
        this.imageUploadService.imageURI !== undefined) {
          const path = this.uploadFile(user.email);
          path.then(imagePath => {
            imagePath.ref.getDownloadURL().then( downloadUrl => {
              this.chiefService.updateChief({image: downloadUrl}, user.email);
              this.router.navigate(['/auth/auth-chief/register-restaurant']);
            });
          });
        } else {
          this.chiefService.updateChief({image: this.imageUploadService.imageFileName}, user.email);
          this.router.navigate(['/auth/auth-chief/register-restaurant']);
        }
      // });
      // console.log(chiefPath);

    })
    .catch(err => this.showToast(err.message));
  }

}
