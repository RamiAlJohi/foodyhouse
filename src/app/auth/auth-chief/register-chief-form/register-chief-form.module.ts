import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterChiefFormPage } from './register-chief-form.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterChiefFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterChiefFormPage]
})
export class RegisterChiefFormPageModule {}
