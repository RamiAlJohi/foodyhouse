import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { PhoneNumber } from '../../../shared/phoneNumber';
import { WindowService } from '../../../services/window.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ChiefService } from '../../../services/chief.service';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Chief } from '../../../shared/chief';
import { NavPop, Platform, ToastController, LoadingController } from '@ionic/angular';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-login-chief-form',
  templateUrl: './login-chief-form.page.html',
  styleUrls: ['./login-chief-form.page.scss'],
})
export class LoginChiefFormPage implements OnInit, OnDestroy {

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  phoneNum: any = '';
  phone = '';
  windowRef: any;
  phoneNumber = new PhoneNumber();
  phoneCtrl = new FormControl('');
  verificationCode: string;
  user: any;
  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthService,
              private win: WindowService, private chiefService: ChiefService,
              private afs: AngularFirestore, private platform: Platform, private toastCtrl: ToastController,
              private loader: LoadingController) {
                this.platform.backButton.subscribe( () => {
                  console.log('back button of the phone has been pressed');
                  this.router.navigate(['/home']);
                });
              }

  ngOnInit() {
    this.isChiefLoggedIn();
    // this.windowRef = this.win.windowRef;
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    // this.windowRef.recaptchaVerifier.render();
  }
  ngOnDestroy(): void {
    this.windowRef = null;
  }

  async presentLoading() {
    const loading = await this.loader.create({
      message: 'الرجاء الانتظار قليلا',
    });
    return await loading.present();
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  isChiefLoggedIn() {
    this.authService.getUser().then( user => {
      if (user) {
        this.presentLoading();
        this.chiefService.getChiefs().pipe(first()).toPromise().then( users => {
        // for (let i = 0; i < users.length; i++) {
        //   if (user && user.uid === users[i].id) {
        //     // console.log(users[i].payload.doc.id);
        //     this.loader.dismiss();
        //     this.router.navigate(['/chief']);
        //   }
        // }
        // this.loader.dismiss();
          let checkExit = '';
          for (let i = 0; i < users.length; i++) {
            const chief: Chief = users[i];
            console.log(i);
            if (user && checkExit === '') {
              checkExit = 'logged in';

            }  if (user.email === users[i].id && checkExit === 'logged in') {
              checkExit = 'half complete';

              console.log(chief.hasOwnProperty('restaurantID'));
              // this.router.navigate(['/chief']);
            }  if (chief.hasOwnProperty('restaurantID')  && checkExit === 'half complete') {
              checkExit = 'complete';
            }
          }

            switch (checkExit) {
              case 'logged in' : {
                console.log(checkExit);
                this.loader.dismiss();
                this.router.navigate(['auth/auth-chief/register-chief']);
              }
              break;
              case 'half complete' : {
                console.log('wtf');
                this.loader.dismiss();
                this.router.navigate(['/auth/auth-chief/register-restaurant']);
              }
              break;
              case 'complete' : {
                this.loader.dismiss();
                this.router.navigate(['/chief']);
              }
          }

        });
      } else {
        this.router.navigate(['auth/auth-chief/login']);
      }
    });

  }

  // newAccount() {
  //   this.router.navigate(['../register'], {relativeTo: this.route});
  // }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  login() {
    console.log(this.loginForm.value);
    this.authService.login(this.loginForm.value)
      .then( user => {
        this.chiefService.getChief(user.user.email).subscribe( chief => {
          if (chief) {
            if (chief.hasOwnProperty('restaurantID')) {
              this.router.navigate(['/chief']);
              console.log('this phone number already exist');
            } else {
              this.router.navigate(['/auth/auth-chief/register-restaurant']);
            }
          } else {
            this.router.navigate(['/auth/auth-chief/register-chief']);
          }
        });
      })
      .catch( err => {
        this.authService.loginErrors(err);
        // this.showToast(err.message)
      });
  }

  register() {
    this.router.navigate(['/auth/register']);
  }

  forgotPassword() {
    this.router.navigate(['/auth/forgot-password']);
  }


  // sendLoginCode() {
  //   // console.log(this.phone);
  //   this.windowRef.signingIn = true;
  //   const appVerifier = this.windowRef.recaptchaVerifier;
  //   const num = '+966' + this.phoneCtrl.value;

  //   firebase.auth().signInWithPhoneNumber(num, appVerifier)
  //           .then(result => {
  //               this.windowRef.confirmationResult = result;

  //           })
  //           .catch( error => {console.log(error); this.showToast(error.message); } );

  // }

  // verifyLoginCode() {
  //   this.windowRef.confirmationResult
  //                 .confirm(this.verificationCode)
  //                 .then( result => {
  //                   console.log('hello');
  //                   this.chiefService.getChief('+966' + this.phoneCtrl.value).pipe(first()).toPromise().then( chief => {
  //                     if (chief) {
  //                       if (chief.hasOwnProperty('restaurantID')) {
  //                         this.router.navigate(['/chief']);
  //                         console.log('this phone number already exist');
  //                       } else {
  //                         this.router.navigate(['/auth/auth-chief/register-restaurant']);
  //                       }
  //                     } else {
  //                       this.router.navigate(['/auth/auth-chief/register-chief']);
  //                     }
  //                   });
  //                   this.user = result.user;
  //   })
  //   .catch( error => {console.log(error, 'Incorrect code entered?'); this.showToast(error.message); });
  // }

}
