import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginChiefFormPage } from './login-chief-form.page';

describe('LoginChiefFormPage', () => {
  let component: LoginChiefFormPage;
  let fixture: ComponentFixture<LoginChiefFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginChiefFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginChiefFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
