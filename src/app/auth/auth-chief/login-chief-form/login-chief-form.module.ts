import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginChiefFormPage } from './login-chief-form.page';

const routes: Routes = [
  {
    path: '',
    component: LoginChiefFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginChiefFormPage]
})
export class LoginChiefFormPageModule {}
