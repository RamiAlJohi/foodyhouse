import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterRestaurantFormPage } from './register-restaurant-form.page';


const routes: Routes = [
  {
    path: '',
    component: RegisterRestaurantFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterRestaurantFormPage]
})
export class RegisterRestaurantFormPageModule {}
