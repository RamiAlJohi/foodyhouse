import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ChiefService } from '../../../../services/chief.service';
import { AuthService } from '../../../../services/auth.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormGroup, FormControl } from '@angular/forms';
import { Marker } from '../../../../shared/marker';
import { Location } from '../../../../shared/location';
import { LoadingService } from '../../../../services/loading.service';

@Component({
  selector: 'app-add-location-restaurant-register',
  templateUrl: './add-location-restaurant-register.page.html',
  styleUrls: ['./add-location-restaurant-register.page.scss'],
})
export class AddLocationRestaurantRegisterPage implements OnInit {
  locationForm = new FormGroup({
    buildingNumber: new FormControl(''),
    apartmentNumber: new FormControl('')
  });

  lat = 21.54238;
  lng = 21.54238;

  markerHome: Marker = {
    lat: 21.54238,
    lng: 21.54238,
    label: '',
    draggable: true
  };

  // markerShop: Marker = {
  //   lat: 51.473858,
  //   lng: 7.815982,
  //   label: '',
  //   draggable: false
  // };

  // markerDriver: Marker = {
  //   lat: 51.573858,
  //   lng: 7.615982,
  //   label: '',
  //   draggable: true
  // };


  constructor(private modalController: ModalController, private geolocation: Geolocation,
              private chiefService: ChiefService, private authService: AuthService, private loadingService: LoadingService) {}

  getLatLon() {
    return this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      this.markerHome.lat = resp.coords.latitude;
      this.markerHome.lng = resp.coords.longitude;
      console.log('latitude = ' + this.markerHome.lat + ', longitude = ' + this.markerHome.lng);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  ngOnInit() {
    this.loadingService.present(5000);
    this.getLatLon().then( () => this.loadingService.dismiss());
  }

  close() {
    this.modalController.dismiss();
  }

  mapClicked($event) {
    this.markerHome.lat = $event.coords.lat;
    this.markerHome.lng = $event.coords.lng;
  }

  locate() {
    this.lat = 51.678418;
    this.lng = 7.809007;
    this.markerHome.lat = 51.678418;
    this.markerHome.lng = 7.809007;
  }

  submit() {
    const location: Location = this.locationForm.value;
    location.lat = this.markerHome.lat;
    location.lng = this.markerHome.lng;
    location.isActive = true;
    console.log('okay');
    this.modalController.dismiss({location: location});
    // this.authService.getUser().then( user => {
      // this.chiefService.updateCustomer({location: location}, user.phoneNumber);
    // });
  }
}
