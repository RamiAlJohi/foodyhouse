import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddLocationRestaurantRegisterPage } from './add-location-restaurant-register.page';


const routes: Routes = [
  {
    path: '',
    component: AddLocationRestaurantRegisterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)

  ],
  declarations: [AddLocationRestaurantRegisterPage]
})
export class AddLocationRestaurantRegisterPageModule {}
