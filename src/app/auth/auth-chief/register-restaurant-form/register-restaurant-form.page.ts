import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChiefService } from '../../../services/chief.service';
import { Restaurant, restStatus } from '../../../shared/restaurant';
import { RestaurantService } from '../../../services/restaurant.service';
import { AuthService } from '../../../services/auth.service';
import { ImageUploadService } from '../../../services/image-upload.service';
import { AddLocationRestaurantRegisterPage } from './add-location-restaurant-register/add-location-restaurant-register.page';
import { ModalController } from '@ionic/angular';
import { Location } from '../../../shared/location';

@Component({
  selector: 'app-register-restaurant-form',
  templateUrl: './register-restaurant-form.page.html',
  styleUrls: ['./register-restaurant-form.page.scss'],
})
export class RegisterRestaurantFormPage implements OnInit {

  registrationForm = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    averagePreparationTime: new FormControl(''),
    workingHours : new FormGroup({
      sunday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
      monday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
      tuesday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
      wensday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
      thursday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
      friday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
      saturday: new FormGroup({
        isOpen: new FormControl(''),
        from: new FormControl('09:00'),
        to: new FormControl('18:00')
      }),
    })
  });
  location: Location;
  constructor(private authService: AuthService, private chiefService: ChiefService, private modalCtrl: ModalController,
              private restaurantService: RestaurantService, private router: Router, private imageUploadService: ImageUploadService) { }

  ngOnInit() {
  }

  getImage() {
    this.imageUploadService.getImage();
  }

  async uploadFile(Id) {
    return await this.imageUploadService.uploadFile(Id);
  }

  async getLocation() {
    const modal = await this.modalCtrl.create({
      component: AddLocationRestaurantRegisterPage
    });
    modal.present();
    modal.onDidDismiss().then( data => {
      console.log(data);
      this.location = data.data.location;
    });
  }

  register() {
    console.log(this.registrationForm.value['workingHours']);
    for (const prop in this.registrationForm.value['workingHours']) {
      if (this.registrationForm.value['workingHours'].hasOwnProperty(prop) ) {
        for (const prop2 in this.registrationForm.value['workingHours'][prop]) {
          if (this.registrationForm.value['workingHours'][prop].hasOwnProperty(prop2)) {
            if (this.registrationForm.value['workingHours'][prop][prop2].hasOwnProperty('hour')) {
              const timeConverter = this.registrationForm.value['workingHours'][prop][prop2]['hour'].text + ':' +
                                    this.registrationForm.value['workingHours'][prop][prop2]['minute'].text;
              this.registrationForm.get('workingHours').get(prop).get(prop2).setValue(timeConverter);
            }
          }
        }
      }
    }
    this.authService.getUser().then( user => {
      console.log(this.chiefService.userEmail);
      const userId = this.chiefService.userEmail;
      let newRestaurant: Restaurant;
      console.log(this.registrationForm.value);
      this.registrationForm.value['rate'] = 0;
      this.registrationForm.value['restaurantStatus'] = restStatus[0];
      newRestaurant = this.registrationForm.value;
      newRestaurant.location = this.location;
      const restaurant = this.restaurantService.newRestaurant(newRestaurant);
      restaurant.then( rest => {
        if (this.imageUploadService.imageURI !== '' && this.imageUploadService.imageURI !== null &&
        this.imageUploadService.imageURI !== undefined) {
          const path = this.uploadFile(rest.id);
          path.then(imagePath => {
            imagePath.ref.getDownloadURL().then( downloadUrl => {
              this.restaurantService.updateRestaurant({image: downloadUrl}, rest.id);
              this.chiefService.updateChief({restaurantID: rest.id}, userId);
              this.router.navigate(['/chief']);
            });
          });
        } else {
          this.restaurantService.updateRestaurant({image: this.imageUploadService.imageFileName}, rest.id);
          this.chiefService.updateChief({restaurantID: rest.id}, userId);
          this.router.navigate(['/chief']);
        }
      });
    });
  }
}
