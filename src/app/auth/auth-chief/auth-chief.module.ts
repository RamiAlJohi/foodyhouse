import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthChiefPage } from './auth-chief.page';
import { LoginChiefFormPage } from './login-chief-form/login-chief-form.page';
import { RegisterChiefFormPage } from './register-chief-form/register-chief-form.page';
import { RegisterRestaurantFormPage } from './register-restaurant-form/register-restaurant-form.page';
// tslint:disable-next-line:max-line-length
import { AddLocationRestaurantRegisterPage } from './register-restaurant-form/add-location-restaurant-register/add-location-restaurant-register.page';
import { AgmCoreModule } from '@agm/core';
import { NgxMaskModule } from 'ngx-mask';

const routes: Routes = [
  {
    path: '',
    component: AuthChiefPage,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginChiefFormPage },
      { path: 'register-chief', component: RegisterChiefFormPage},
      { path: 'register-restaurant', component: RegisterRestaurantFormPage}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8fwBCtKMdrlBa_mC8dqLQSncyBdpCYy4'
    })

  ],
  declarations: [AuthChiefPage, LoginChiefFormPage, RegisterChiefFormPage, RegisterRestaurantFormPage, AddLocationRestaurantRegisterPage],
  entryComponents: [AddLocationRestaurantRegisterPage]
})
export class AuthChiefPageModule {}
