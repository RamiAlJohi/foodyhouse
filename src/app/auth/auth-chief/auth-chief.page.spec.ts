import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthChiefPage } from './auth-chief.page';

describe('AuthChiefPage', () => {
  let component: AuthChiefPage;
  let fixture: ComponentFixture<AuthChiefPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthChiefPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthChiefPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
