import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { ImageUploadService } from '../../../services/image-upload.service';
import { ChiefService } from '../../../services/chief.service';
import { Customer } from '../../../shared/customer';
import { ModalController, LoadingController, ToastController } from '@ionic/angular';
import { Location } from '../../../shared/location';
import { AddLocationCustomerRegisterPage } from './add-location-customer-register/add-location-customer-register.page';
import { first } from 'rxjs/operators';
// import { AddLocationCustomerRegisterPage } from './add-location-customer-register/add-location-customer-register.page';

@Component({
  selector: 'app-register-customer-form',
  templateUrl: './register-customer-form.page.html',
  styleUrls: ['./register-customer-form.page.scss'],
})
export class RegisterCustomerFormPage implements OnInit {

  registrationForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    mobile: new FormControl(''),
  });
  location: Location;

  constructor(private authService: AuthService, private router: Router,
              private chiefService: ChiefService, private imageUploadService: ImageUploadService, private modalCtrl: ModalController,
              private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  getImage() {
    this.imageUploadService.getImage();
  }

  async uploadFile(Id) {
    return await this.imageUploadService.uploadFile(Id);
  }

  async getLocation() {
    const modal = await this.modalCtrl.create({
      component: AddLocationCustomerRegisterPage
    });
    modal.present();
    modal.onDidDismiss().then( data => {
      console.log(data);
      this.location = data.data.location;
    });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPass').value;

    return pass === confirmPass ||  pass === '' || confirmPass === '' ? null : { notSame: true };
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  register() {
    const customer: Customer = this.registrationForm.value;
    customer.orderingSessionID = '';
    this.authService.getUser().then( user => {
      customer.email = user.email;
      customer.location = this.location;
      const customerID = this.chiefService.addCustomer(customer, user.email);
      if (this.imageUploadService.imageURI !== '' && this.imageUploadService.imageURI !== null &&
      this.imageUploadService.imageURI !== undefined) {
          const path = this.uploadFile(user.email);
          path.then(imagePath => {
            imagePath.ref.getDownloadURL().then( downloadUrl => {
              this.chiefService.updateCustomer({image: downloadUrl}, user.email);
              this.router.navigate(['/customer']);
            });
          });
        }
      })
      .catch(err => this.showToast(err.message));
  }
}
