import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterCustomerFormPage } from './register-customer-form.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterCustomerFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterCustomerFormPage]
})
export class RegisterCustomerFormPageModule {}
