import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddLocationCustomerRegisterPage } from './add-location-customer-register.page';

const routes: Routes = [
  {
    path: '',
    component: AddLocationCustomerRegisterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddLocationCustomerRegisterPage]
})
export class AddLocationCustomerRegisterPageModule {}
