import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthCustomerPage } from './auth-customer.page';
import { LoginCustomerFormPage } from './login-customer-form/login-customer-form.page';
import { RegisterCustomerFormPage } from './register-customer-form/register-customer-form.page';
// tslint:disable-next-line:max-line-length
import { AddLocationCustomerRegisterPage } from './register-customer-form/add-location-customer-register/add-location-customer-register.page';
import { AgmCoreModule } from '@agm/core';
import { NgxMaskModule } from 'ngx-mask';

const routes: Routes = [
  {
    path: '',
    component: AuthCustomerPage,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginCustomerFormPage },
      { path: 'register-customer', component: RegisterCustomerFormPage}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8fwBCtKMdrlBa_mC8dqLQSncyBdpCYy4'
    })
  ],
  declarations: [AuthCustomerPage, LoginCustomerFormPage, RegisterCustomerFormPage, AddLocationCustomerRegisterPage],
  entryComponents: [AddLocationCustomerRegisterPage]
})
export class AuthCustomerPageModule {}
