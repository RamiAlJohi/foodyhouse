import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ChiefService } from '../../../services/chief.service';
import { WindowService } from '../../../services/window.service';
import { PhoneNumber } from '../../../shared/phoneNumber';
import * as firebase from 'firebase';
import { RestaurantListService } from '../../../services/restaurant-list.service';
import { Observable, Subscription } from 'rxjs';
import { Chief } from '../../../shared/chief';
import { Customer } from '../../../shared/customer';
import { FormGroup, FormControl } from '@angular/forms';
import { Platform, ToastController, LoadingController } from '@ionic/angular';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login-customer-form',
  templateUrl: './login-customer-form.page.html',
  styleUrls: ['./login-customer-form.page.scss'],
})
export class LoginCustomerFormPage implements OnInit, OnDestroy {

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  phone = '';
  windowRef: any;
  phoneNumber = new PhoneNumber();
  verificationCode: string;
  user: any;
  chiefServiceObservable: Subscription;
  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthService,
              private win: WindowService, private chiefService: ChiefService, private afs: AngularFirestore,
               private platform: Platform, private toastCtrl: ToastController, private loader: LoadingController) {
                this.platform.backButton.subscribe( () => {
                  console.log('back button of the phone has been pressed');
                  this.router.navigate(['/home']);
                });
              }

  ngOnInit() {
    this.checkUserLoggedIn();
    // this.windowRef = this.win.windowRef;
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    // this.windowRef.recaptchaVerifier.render();
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  ngOnDestroy(): void {
    console.log('okay good');
  }

  async presentLoading() {
    const loading = await this.loader.create({
      message: 'يتم تسجيل الدخول',
      duration: 3000
    });
    return await loading.present();
  }

  checkUserLoggedIn() {
    this.authService.getUser().then( user => {
      if (user) {
        this.presentLoading();
        this.chiefService.getCustomers().pipe(first()).toPromise().then( users => {
          console.log('isCustomerLoggIn How');
          for (let i = 0; i < users.length; i++) {
            if (user && user.email === users[i].payload.doc.id) {
              console.log(users[i].payload.doc.id);
              this.loader.dismiss();
              this.router.navigate(['/customer']);
            }
          }
          this.loader.dismiss();
        });
      }
    });
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  login() {
    console.log(this.loginForm.value);
    this.authService.login(this.loginForm.value)
      .then( user => {
        console.log(user.user.email);
          this.chiefService.getCustomerValue(user.user.email).subscribe( customer => {
              if (customer) {
                this.router.navigate(['/customer']);
                console.log(customer);
              } else {
                this.router.navigate(['/auth/auth-customer/register-customer']);
              }
          });
      })
      .catch( err => this.authService.loginErrors(err));
  }

  register() {
    this.router.navigate(['/auth/register']);
  }

  forgotPassword() {
    this.router.navigate(['/auth/forgot-password']);
  }

  // sendLoginCode() {
  //   console.log(this.phone);
  //   const appVerifier = this.windowRef.recaptchaVerifier;

  //   const num = '+966' + this.phoneCtrl.value;

  //   firebase.auth().signInWithPhoneNumber(num, appVerifier)
  //           .then(result => {
  //               this.windowRef.confirmationResult = result;

  //           })
  //           .catch( error => {console.log(error); this.showToast(error.message); } );

  // }

  // verifyLoginCode() {
  //   this.windowRef.confirmationResult
  //                 .confirm(this.verificationCode)
  //                 .then( result => {
  //                   console.log('hello');
  //                   this.chiefService.getCustomerValue('+966' + this.phoneCtrl.value).subscribe( chief => {
  //                     if (chief) {
  //                       this.router.navigate(['/customer']);
  //                       console.log('this phone number already exist');
  //                     } else {
  //                       this.router.navigate(['/auth/auth-customer/register-customer']);
  //                     }
  //                   });
  //                   this.user = result.user;
  //   })
  //   .catch( error => {console.log(error, 'Incorrect code entered?'); this.showToast(error.message); });
  // }
}
