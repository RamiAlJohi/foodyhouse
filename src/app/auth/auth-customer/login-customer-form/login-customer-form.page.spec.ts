import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCustomerFormPage } from './login-customer-form.page';

describe('LoginCustomerFormPage', () => {
  let component: LoginCustomerFormPage;
  let fixture: ComponentFixture<LoginCustomerFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginCustomerFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCustomerFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
