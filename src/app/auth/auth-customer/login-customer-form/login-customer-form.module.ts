import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginCustomerFormPage } from './login-customer-form.page';

const routes: Routes = [
  {
    path: '',
    component: LoginCustomerFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginCustomerFormPage]
})
export class LoginCustomerFormPageModule {}
