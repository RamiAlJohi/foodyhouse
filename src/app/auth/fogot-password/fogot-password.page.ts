import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Location } from '@angular/common';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-fogot-password',
  templateUrl: './fogot-password.page.html',
  styleUrls: ['./fogot-password.page.scss'],
})
export class FogotPasswordPage implements OnInit {
  resetPasswordForm = new FormGroup({
    email: new FormControl('')
  });
  constructor(private authService: AuthService, private location: Location, private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  forgotPasswordSubmit() {
    const email: string = this.resetPasswordForm.value.email;
    this.authService.resetPassword(email).catch( err => this.authService.forgetPasswordError(err));
  }

  goBack() {
    this.location.back();
  }

}
