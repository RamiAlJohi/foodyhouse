import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthDeliveryPage } from './auth-delivery.page';
import { LoginDeliveryFormPage } from './login-delivery-form/login-delivery-form.page';
import { RegisterDeliveryFormPage } from './register-delivery-form/register-delivery-form.page';
import { NgxMaskModule } from 'ngx-mask';

const routes: Routes = [
  {
    path: '',
    component: AuthDeliveryPage,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginDeliveryFormPage },
      { path: 'register-delivery', component: RegisterDeliveryFormPage},
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [AuthDeliveryPage, LoginDeliveryFormPage, RegisterDeliveryFormPage]
})
export class AuthDeliveryPageModule {}
