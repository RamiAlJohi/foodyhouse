import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginDeliveryFormPage } from './login-delivery-form.page';

describe('LoginDeliveryFormPage', () => {
  let component: LoginDeliveryFormPage;
  let fixture: ComponentFixture<LoginDeliveryFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginDeliveryFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginDeliveryFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
