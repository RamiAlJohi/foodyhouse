import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginDeliveryFormPage } from './login-delivery-form.page';

const routes: Routes = [
  {
    path: '',
    component: LoginDeliveryFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginDeliveryFormPage]
})
export class LoginDeliveryFormPageModule {}
