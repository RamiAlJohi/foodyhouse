import { Component, OnInit } from '@angular/core';
import { WindowService } from '../../../services/window.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PhoneNumber } from '../../../shared/phoneNumber';
import { AuthService } from '../../../services/auth.service';
import { ChiefService } from '../../../services/chief.service';
import * as firebase from 'firebase';
import { DeliveryService } from '../../../services/delivery.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Platform, ToastController, LoadingController } from '@ionic/angular';
import { tap, first } from 'rxjs/operators';

@Component({
  selector: 'app-login-delivery-form',
  templateUrl: './login-delivery-form.page.html',
  styleUrls: ['./login-delivery-form.page.scss'],
})
export class LoginDeliveryFormPage implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  phone = '';
  windowRef: any;
  phoneNumber = new PhoneNumber();
  phoneCtrl = new FormControl('');
  verificationCode: string;
  user: any;
  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthService,
              private win: WindowService, private deliveryService: DeliveryService, private platform: Platform,
              private toastCtrl: ToastController, private loader: LoadingController) {
                this.platform.backButton.subscribe( () => {
                  console.log('back button of the phone has been pressed');
                  this.router.navigate(['/home']);
                });
              }

  ngOnInit() {
    this.checkUserLoggedIn();
    // this.windowRef = this.win.windowRef;
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    // this.windowRef.recaptchaVerifier.render();
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  async presentLoading() {
    const loading = await this.loader.create({
      message: 'الرجاء الانتظار قليلا',
    });
    return await loading.present();
  }

  checkUserLoggedIn() {
    this.authService.getUser().then( user => {
      if (user) {
        this.presentLoading();
        this.deliveryService.getDeliveryGuys().pipe(first()).toPromise().then( users => {
          // console.log('isCustomerLoggIn How');
          for (let i = 0; i < users.length; i++) {
            if (user && user.email === users[i].payload.doc.id) {
              console.log('okay find peace of shit ' + users[i].payload.doc.id);
              this.loader.dismiss();
              this.router.navigate(['/delivery']);
            }
          }
          this.loader.dismiss();
        });
      }
    });
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  login() {
    console.log(this.loginForm.value);
    this.authService.login(this.loginForm.value)
      .then( user => {

        this.deliveryService.getDeliveryGuyValue(user.user.email).subscribe( delivery => {
          if (delivery) {
            // console.log('please work');
            this.router.navigate(['/delivery']);
            // console.log('this phone number already exist');
          } else {
            this.router.navigate(['/auth/auth-delivery/register-delivery']);
          }
        });
      })
      .catch( err => this.authService.loginErrors(err));
  }

  register() {
    this.router.navigate(['/auth/register']);
  }

  forgotPassword() {
    this.router.navigate(['/auth/forgot-password']);
  }

  // sendLoginCode() {
  //   console.log('shit' + this.phoneCtrl.value);
  //   const appVerifier = this.windowRef.recaptchaVerifier;

  //   const num = '+966' + this.phoneCtrl.value;

  //   firebase.auth().signInWithPhoneNumber(num, appVerifier)
  //           .then(result => {
  //               this.windowRef.confirmationResult = result;

  //           })
  //           .catch( error => {console.log(error); this.showToast(error.message); } );

  // }

  // verifyLoginCode() {
  //   this.windowRef.confirmationResult
  //                 .confirm(this.verificationCode)
  //                 .then( result => {
  //                   console.log('hello');
  //                   this.deliveryService.getDeliveryGuyValue('+966' + this.phoneCtrl.value).pipe(first()).toPromise().then( deliveryGuy => {
  //                     if (deliveryGuy) {
  //                       console.log('please work');
  //                       this.router.navigate(['/delivery']);
  //                       console.log('this phone number already exist');
  //                     } else {
  //                       this.router.navigate(['/auth/auth-delivery/register-delivery']);
  //                     }
  //                   });
  //                   this.user = result.user;
  //   })
  //   .catch( error => {console.log(error, 'Incorrect code entered?'), this.showToast(error.message); });
  // }
}
