import { Component, OnInit } from '@angular/core';
import { Delivery } from '../../../shared/delivery';
import { DeliveryService } from '../../../services/delivery.service';
import { ImageUploadService } from '../../../services/image-upload.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import { DeliveryGuy } from '../../../shared/deliveryguy';
import { ToastController } from '@ionic/angular';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-register-delivery-form',
  templateUrl: './register-delivery-form.page.html',
  styleUrls: ['./register-delivery-form.page.scss'],
})
export class RegisterDeliveryFormPage implements OnInit {

  registrationForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    mobile: new FormControl(''),
  });

  constructor(private authService: AuthService, private router: Router,
              private deliveryService: DeliveryService, private imageUploadService: ImageUploadService,
              private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  getImage() {
    this.imageUploadService.getImage();
  }

  async uploadFile(Id) {
    return await this.imageUploadService.uploadFile(Id);
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPass').value;

    return pass === confirmPass ||  pass === '' || confirmPass === '' ? null : { notSame: true };
  }

  register() {
    const deliveryGuy: DeliveryGuy = this.registrationForm.value;
    // const password = this.registrationForm.value.passwords.password;
    // this.authService.getUser().then( user => {
      this.authService.getUser().then( user => {
        deliveryGuy.email = user.email;
        const deliveryGuyID = this.deliveryService.addDeliveryGuy(deliveryGuy, user.email);
        // deliveryGuyID.valueChanges().pipe(first()).toPromise().then( deliveryGuyInfo => {
          if (this.imageUploadService.imageURI !== '' && this.imageUploadService.imageURI !== null &&
          this.imageUploadService.imageURI !== undefined) {
            const path = this.uploadFile(user.email);
            path.then(imagePath => {
              imagePath.ref.getDownloadURL().then( downloadUrl => {
                this.deliveryService.updateDeliveryGuy(user.email, {image: downloadUrl});
                this.router.navigate(['/delivery']);
              });
            });
          } else {
            this.deliveryService.updateDeliveryGuy(user.email, {image: this.imageUploadService.imageFileName});
            this.router.navigate(['/delivery']);
          }
        // });
      })
      .catch(err => this.showToast(err.message));
    // });
  }
}
