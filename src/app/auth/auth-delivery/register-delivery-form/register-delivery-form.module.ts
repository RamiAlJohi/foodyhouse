import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterDeliveryFormPage } from './register-delivery-form.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterDeliveryFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterDeliveryFormPage]
})
export class RegisterDeliveryFormPageModule {}
