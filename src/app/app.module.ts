import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, ToastController, Nav, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { Geolocation } from '@ionic-native/geolocation/ngx';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FcmService } from './services/fcm.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RestaurantListService } from './services/restaurant-list.service';
import { WindowService } from './services/window.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { ChiefService } from './services/chief.service';
import { HomePage } from './home/home.page';
import { ImageUploadService } from './services/image-upload.service';
import { Firebase } from '@ionic-native/firebase/ngx';
import { PreviousRouteService } from './services/previous-route.service';
import { DeliveryService } from './services/delivery.service';
import { AuthService } from './services/auth.service';
import { LoadingService } from './services/loading.service';



@NgModule({
  declarations: [AppComponent, HomePage],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireMessagingModule,
    AngularFireFunctionsModule,
    AngularFireAuthModule,
    AngularFireStorageModule,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Geolocation,
    ChiefService,
    DeliveryService,
    FcmService,
    ToastController,
    RestaurantListService,
    WindowService,
    PreviousRouteService,
    Camera,
    AuthService,
    ImageUploadService,
    Firebase,
    LoadingService,
    LoadingController
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
