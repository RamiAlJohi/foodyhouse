import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { HomePage } from './home/home.page';



const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePage},

  { path: 'customer', loadChildren: './customer/customer.module#CustomerPageModule' },
  { path: 'delivery', loadChildren: './delivery/delivery.module#DeliveryPageModule' },
  { path: 'chief', loadChildren: './chief/chief.module#ChiefPageModule'},

  {
    path: 'auth',
    children: [
      {path: 'auth-chief', loadChildren: './auth/auth-chief/auth-chief.module#AuthChiefPageModule'},
      {path: 'auth-customer', loadChildren: './auth/auth-customer/auth-customer.module#AuthCustomerPageModule'},
      {path: 'auth-delivery', loadChildren: './auth/auth-delivery/auth-delivery.module#AuthDeliveryPageModule'},
      { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' },
      { path: 'forgot-password', loadChildren: './auth/fogot-password/fogot-password.module#FogotPasswordPageModule' }
    ],

}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
