import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, first } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private af: AngularFireAuth, private router: Router, private toastCtrl: ToastController,
              private authService: AuthService) {}

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    return await toast.present();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.af.user.pipe(map((auth) =>  {
        console.log(auth.emailVerified);
        if (auth.emailVerified === false) {
          this.router.navigate(['/home']);
          this.authService.userSignOut();
          this.showToast('لم يتم التحقق من الأميل الرجاء التحقق');
          return false;
        } else {
          return true;
        }
      }), first());
  }
}
