import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Restaurant } from '../../shared/restaurant';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { Router } from '@angular/router';
import { AlertController, Platform, LoadingController } from '@ionic/angular';
import { PreviousRouteService } from '../../services/previous-route.service';
import { FcmService } from '../../services/fcm.service';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';
import { InfiniteScroll } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { ChiefService } from '../../services/chief.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.page.html',
  styleUrls: ['./restaurant-list.page.scss'],
})
export class RestaurantListPage implements OnInit, OnDestroy {

  restaurants: Restaurant[]; // = RESTAURANTS;

  searchVal = '';
  sorts = ['rating', 'name'];
  sortVal: string = this.sorts[0];
  totalNumOfCustomers = 0;
  dataLimit = 5;
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  constructor(private restaurantListService: RestaurantListService, private router: Router, public alertController: AlertController,
              private platform: Platform, private previousRouteService: PreviousRouteService, private fcm: FcmService,
              private authService: AuthService, private loadingService: LoadingService, private chiefService: ChiefService) {
    if (this.previousRouteService.getPreviousUrl() !== '/auth/auth-customer/login' ||
        this.previousRouteService.getPreviousUrl() !== '/auth/auth-customer/register-customer') {
      this.platform.backButton.subscribe( () => {
        this.router.navigate([this.previousRouteService.getPreviousUrl()]);
      });
    }
  }

  ngOnInit() {
    this.loadingService.present(5000);
    this.chiefService.getRestaurantsValue().subscribe( restaurants => {
      this.totalNumOfCustomers = restaurants.length;
    });
    this.restaurantListService.getRestaurantsSnap(this.dataLimit)
    .subscribe(restaurants => {
      // this.isLoadingData = true;
      this.restaurants = restaurants;
      for (const i of this.restaurants) {
        this.restaurantListService.getNumberOfReviews(i.id)
        .subscribe(number => {i.numberOfRates = number;  });
      }
      // this.isLoadingData = false;
      this.loadingService.dismiss();
    });
    this.fcm.getPremission().subscribe( () => {
      if (this.fcm.token != null) {
        this.authService.getUser().then( user => {
          const customerId = user.email;
          const newCustomerId = user.email.replace('@', '~');
          console.log(this.fcm.token);
          console.log(customerId.slice(customerId.indexOf('+') + 1));
          this.fcm.sub('customer' + newCustomerId);
        });
      }
    });
    this.fcm.listenToMessages().subscribe();
  }

  sortChange(evn) {
    this.sortVal === this.sorts[1] ?
    this.restaurants.sort((x, y) => x.name < y.name ? -1 : 1) : this.restaurants.sort((x, y) => x.rate > y.rate ? -1 : 1);
  }

  toRestaurant(restaurantID: string, status: string) {
    if (status === 'open') {
      this.router.navigate(['/customer/menu', restaurantID]);
    } else if (status === 'busy') {
      this.presentAlert('مشغول');
    } else {this.presentAlert('مغلق'); }
  }

  async presentAlert(status: string) {
    const alert = await this.alertController.create({
      header: 'عذرا المطعم ' + status + '..',
      buttons: ['حسنا']
    });

    await alert.present();
  }

  ngOnDestroy(): void {
    console.log('RestaurantListPage Destroyed');
  }

  loadData(event) {
    this.dataLimit += 5;
    console.log(this.dataLimit);
    setTimeout( () => {
      this.restaurantListService.getRestaurantsSnap(this.dataLimit)
      .subscribe(restaurants => {
        // this.isLoadingData = true;
        for (let i = this.restaurants.length; i < restaurants.length; i++) {
          this.restaurants.push(restaurants[i]);
        }
        // this.restaurants = restaurants;
        for (const i of this.restaurants) {
          this.restaurantListService.getNumberOfReviews(i.id)
          .subscribe(number => {i.numberOfRates = number;  });
        }
        // this.isLoadingData = false;
        event.target.complete();
        if (this.restaurants.length >= this.totalNumOfCustomers) {
          event.target.disabled = true;
        }
        // this.loadingService.dismiss();
      });
    }, 1000);

  }

  

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000
  //   });
  //   return await loading.present();
  // }
}
