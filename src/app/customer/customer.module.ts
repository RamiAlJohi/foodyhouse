import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustomerPage } from './customer.page';
import { RestaurantListPage } from './restaurant-list/restaurant-list.page';
import { MenuPage } from './menu/menu.page';
import { LocationPage } from './location/location.page';
import { CartPage } from './cart/cart.page';
import { BillPage } from './bill/bill.page';
import { AddLocationPage } from './add-location/add-location.page';
import { RestaurantDetailsPage } from './restaurant-details/restaurant-details.page';
import { AgmCoreModule } from '@agm/core';
import { CustomerProfilePage } from './customer-profile/customer-profile.page';
import { AuthGuard } from '../guards/auth.guard';


const routes: Routes = [
  {
    path: '',
    component: CustomerPage,
    children: [
      { path: '', redirectTo: 'restaurantList', pathMatch: 'full'},
      { path: 'restaurantList', component: RestaurantListPage, canActivate: [AuthGuard] },
      { path: 'menu/:id', loadChildren: './menu/menu.module#MenuPageModule', canActivate: [AuthGuard] },
      { path: 'location', loadChildren: './location/location.module#LocationPageModule', canActivate: [AuthGuard] },
      { path: 'profile', loadChildren: './customer-profile/customer-profile.module#CustomerProfilePageModule', canActivate: [AuthGuard] },
      { path: 'cart', loadChildren: './cart/cart.module#CartPageModule', canActivate: [AuthGuard] },
      // { path: 'notifications', loadChildren: './customer-notification/customer-notification.module#CustomerNotificationPageModule',
      // canActivate: [AuthGuard] }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8fwBCtKMdrlBa_mC8dqLQSncyBdpCYy4'
    })
  ],
  declarations: [CustomerPage, RestaurantListPage,
                    BillPage, AddLocationPage, RestaurantDetailsPage],
  entryComponents: [BillPage, AddLocationPage, RestaurantDetailsPage]
})
export class CustomerPageModule {}
