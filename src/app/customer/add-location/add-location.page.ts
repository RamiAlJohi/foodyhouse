import { Component, OnInit, Input } from '@angular/core';

import { ModalController, Platform, LoadingController } from '@ionic/angular';
import { Marker } from '../../shared/marker';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Location } from '../../shared/location';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';


@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.page.html',
  styleUrls: ['./add-location.page.scss'],
})
export class AddLocationPage implements OnInit {

  @Input() dd: any;

  lat = 21.54238;
  lng = 21.54238;

  markerHome: Marker = {
    lat: 21.54238,
    lng: 21.54238,
    label: '',
    draggable: true
  };

  buildingNumber = '';
  apartmentNumber = '';

  constructor(private modalController: ModalController, private geolocation: Geolocation, public loadingService: LoadingService,
              private restaurantListService: RestaurantListService, private authService: AuthService, private platform: Platform) {
                this.platform.backButton.subscribe( () => {
                  this.modalController.dismiss();
                });
              }

  getLatLon() {
    return this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      this.markerHome.lat = resp.coords.latitude;
      this.markerHome.lng = resp.coords.longitude;
      console.log('latitude = ' + this.markerHome.lat + ', longitude = ' + this.markerHome.lng);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  ngOnInit() {
    this.loadingService.present(5000);
    this.getLatLon().then(latLng => this.loadingService.dismiss());
  }

  close() {
    this.modalController.dismiss();
  }

  mapClicked($event) {
    this.markerHome.lat = $event.coords.lat;
    this.markerHome.lng = $event.coords.lng;
  }

  locate() {
    this.lat = 51.678418;
    this.lng = 7.809007;
    this.markerHome.lat = 51.678418;
    this.markerHome.lng = 7.809007;
  }

  confirm() {
    const location: Location = {
      lat: this.markerHome.lat,
      lng: this.markerHome.lng,
      isActive: true
    };
    if (this.buildingNumber !== '') {location.buildingNumber = this.buildingNumber; }
    if (this.apartmentNumber !== '') {location.apartmentNumber = this.apartmentNumber; }
    console.log(this.buildingNumber);
    this.authService.getUser()
    .then(auth => {
      console.log('try: ' + auth.email);
      this.restaurantListService.updateCustomerLocation(auth.email, location)
      .then(whatever => {console.log('wtf'); this.close(); });
    });
  }


  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000
  //   });
  //   return await loading.present();
  // }
}
