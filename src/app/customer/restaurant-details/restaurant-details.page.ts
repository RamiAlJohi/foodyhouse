import { Component, OnInit, Input } from '@angular/core';
import { ModalController, Platform, Nav } from '@ionic/angular';

import { Restaurant } from '../../shared/restaurant';
import { Time } from '@angular/common';
import { Day } from '../../shared/Day';
import { Review } from '../../shared/review';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { AuthService } from '../../services/auth.service';
// import { RESTAURANTS } from '../../shared/restaurants';

@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.page.html',
  styleUrls: ['./restaurant-details.page.scss'],
})
export class RestaurantDetailsPage implements OnInit {

  @Input() restaurant: Restaurant;

  reviews: Review[];
  review: Review = {
    name: '',
    date: '',
    rate: 0,
    comment: ''
  };

  constructor(private modalController: ModalController, private restaurantListService: RestaurantListService,
    private authService: AuthService, private platform: Platform) {
      this.platform.backButton.subscribe( () => {
        this.modalController.dismiss();
      });
    }

  ngOnInit() {
    this.restaurantListService.getCommentsVal(this.restaurant.id)
    .subscribe(reviews => this.reviews = reviews);
    this.authService.getUser()
    .then(auth => {
      console.log('try: ' + auth.email);
      this.restaurantListService.getCustomer(auth.email)
      .subscribe(customer => this.review.name = customer.firstName + ' ' + customer.lastName);
    });
  }

  changeRate(rate: number) {
    this.review.rate = rate;
  }

  submit() {
    this.review.date = new Date(Date.now()).toISOString();
    console.log(this.restaurant.id);
    console.log(this.review);
    this.restaurantListService.addReview(this.restaurant.id, this.review)
    .then(whatever => {
      const rate = this.calculateRating(this.reviews);
      console.log(rate);
      this.restaurantListService.updateRating(this.restaurant.id, rate);
    });
  }

  calculateRating(reviews: Review[]): number {
    let total = 0;
    for (const i of reviews) {
      total += i.rate;
    }
    return (reviews.length !== 0) ?  total / reviews.length : 0;
  }

  close() {
    this.modalController.dismiss();
  }
}
