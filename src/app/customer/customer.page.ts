import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FcmService } from '../services/fcm.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage implements OnInit {

  public appPages = [
    {
      title: 'المطاعم',
      url: './restaurantList',
      icon: 'ios-restaurant'
    },
    {
      title: 'الموقع',
      url: './location',
      icon: 'ios-map'
    },
    {
      title: 'أنا',
      url: './profile',
      icon: 'ios-person'
    },
    {
      title: 'الطلبات',
      url: './cart',
      icon: 'ios-cart'
    },
    {
      title: 'تغيير الحساب',
      url: '/home',
      icon: 'ios-home'
    },
    // {
    //   title: 'التنبيهات',
    //   url: './notifications',
    //   icon: 'ios-alert'
    // },
  ];

  constructor(private authService: AuthService, private router: Router, private fcm: FcmService) { }

  ngOnInit() {
  }

  signOut() {
    this.authService.getUser().then( user => {
      const customerId = user.email.replace('@', '~');
      this.fcm.unsub('customer' + customerId);
      console.log('customer' + customerId);
      this.authService.userSignOut().then(() =>
        this.router.navigate(['/home'])
      );
    });
  }
}
