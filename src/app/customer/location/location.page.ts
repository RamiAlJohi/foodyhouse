import { Component, OnInit } from '@angular/core';

import { ModalController, Platform, Nav } from '@ionic/angular';
import { AddLocationPage } from '../add-location/add-location.page';
import { RestaurantListService } from '../../services/restaurant-list.service';

import { Location } from '../../shared/location';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';


@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {

  location: Location;

  constructor(public modalController: ModalController, private restaurantListService: RestaurantListService,
    private authService: AuthService, private platform: Platform, private router: Router,
    private previousRouteService: PreviousRouteService) {
      this.platform.backButton.subscribe( () => {
        this.router.navigate([this.previousRouteService.getPreviousUrl()]);
      });
    }

  ngOnInit() {
    this.authService.getUser()
    .then(auth => {
      console.log(auth.email);
      this.restaurantListService.getCustomer(auth.email)
      .subscribe(customer => this.location = customer.location);
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddLocationPage,
      componentProps: { }
    });
    return await modal.present();
  }



}
