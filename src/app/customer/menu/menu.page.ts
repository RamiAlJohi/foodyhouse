import { Component, OnInit } from '@angular/core';

import { ModalController, LoadingController, Platform, Nav } from '@ionic/angular';
import { BillPage } from '../bill/bill.page';
import { Item, CATEGORIES } from '../../shared/item';
import { RestaurantDetailsPage } from '../restaurant-details/restaurant-details.page';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Restaurant } from '../../shared/restaurant';
import { ORDERSTATUS, Order } from '../../shared/order';
import { first } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
import { PreviousRouteService } from '../../services/previous-route.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  customerID = 'Pk2oW0HDo2FUIJOM0jP0'; // get it from auth later
  restaurant: Restaurant;
  items: Item[];
  category: string; // used in html/segmants
  added = 0;
  isOrderItemsExist = true;
  restaurantID: string;

  restSnapSub: any;
  orderingSessionID: string;

  categories: string[] = CATEGORIES;

  constructor(public modalController: ModalController, private activatedRoute: ActivatedRoute,
              private restaurantListService: RestaurantListService, private authService: AuthService,
              public loadingService: LoadingService, private platform: Platform,
              private previousRouteService: PreviousRouteService, private router: Router) {
                this.platform.backButton.subscribe( () => {
                  this.router.navigate([this.previousRouteService.getPreviousUrl()]);
                });
              }

  ngOnInit() {
    this.loadingService.present(5000);
    this.activatedRoute.params
    .subscribe( param => {
      this.restaurantID = param.id;
      this.restaurantListService.getRestaurantSnap(param.id)
      .subscribe(restaurant => {
        this.restaurant = restaurant;
        console.log(restaurant.id);

        this.restaurantListService.getItemsSnap(restaurant.id)
        .subscribe(items => {this.items = items; this.loadingService.dismiss(); });
      });

      // this.checkingSessionAndAddToOrder(param.id);
    });
  }

  checkingSessionAndAddToOrder(itemID: string) {
    if (this.restaurantID) {
      this.loadingService.present(5000);
      this.authService.getUser()
      .then(auth => {
        this.restaurantListService.getCustomer(auth.email)
        .pipe(first()).toPromise().then(customer => {
          this.restaurantListService.getItemSnap(this.restaurantID, itemID)
          .pipe(first()).toPromise().then(item => {
            if (customer.orderingSessionID === undefined || customer.orderingSessionID === null || customer.orderingSessionID === '') {
              console.log('session is undefined');
              this.restaurantListService.createOrderForSession(auth.email, this.restaurantID)
              .then(addedOrder => {
                this.restaurantListService.updateOrderSessionID(auth.email, addedOrder.id)
                .then(updatedOrderSessionID => this.addtoOrder(addedOrder.id, item)); // <= add orderItem
              });
            } else {
              this.restaurantListService.getOrderSnap(customer.orderingSessionID)
              .pipe(first()).toPromise().then(order => {
                if (order.restaurantID) {
                  if (order.restaurantID !==  this.restaurantID) { // delete is dangrous
                    console.log('clear order from other restaurant');
                    this.restaurantListService.deleteOrderItems(order.id)
                    .then(deletedOrderItems => {
                      Promise.all(deletedOrderItems)
                      .then(all => {
                        this.restaurantListService.assignRestaurantIDToSession(customer.orderingSessionID, this.restaurantID)
                        .then(zz => {
                          console.log('assigned Item ID');
                          this.addtoOrder(customer.orderingSessionID, item);
                        });
                      });
                    });
                  } else {
                    this.addtoOrder(customer.orderingSessionID, item);
                    // .then(added => this.modalController.dismiss());
                  }
                } else {
                  console.log('order not found');
                  this.restaurantListService.createOrderForSession(auth.email, this.restaurantID)
                  .then(addedOrder => {
                    this.restaurantListService.updateOrderSessionID(auth.email, addedOrder.id)
                    .then(updatedOrderSessionID => this.addtoOrder(addedOrder.id, item)); // <= add orderItem
                  });
                }
              });
            }
          });
        });
      });
    }
  }

  async billModal() {
    const modal = await this.modalController.create({
      component: BillPage,
      componentProps: {  }
    });
    return await modal.present();
  }

  async restaurantDetailsModal() {
    const modal = await this.modalController.create({
      component: RestaurantDetailsPage,
      componentProps: { restaurant: this.restaurant }
    });
    return await modal.present();
  }

  addtoOrder(orderingSessionID: string, item: Item) {
    return this.restaurantListService.getOrderItems(orderingSessionID)
    .pipe(first()).toPromise().then(orderItems => {
      const orderItem = orderItems.filter(forderItem => forderItem.item.id === item.id)[0];
      if (orderItem) {
        console.log(item.id + ', updated quantity');
        return this.restaurantListService.updateQuantity(orderingSessionID, orderItem.id, orderItem.quantity + 1)
        .then(updatedQuantity => {this.loadingService.dismiss(); console.log('updated quantity'); });
      } else {
        return this.restaurantListService.addOrderItemToSession(orderingSessionID, item)
        .then(added => { this.added++; this.loadingService.dismiss(); console.log('OrderItemAdded'); });
      }
    });
  }

  // addtoOrder(itemID: string) {
  //   console.log(itemID);
  //   this.authService.getUser()
  //   .then(auth => {
  //     console.log('try: ' + auth.phoneNumber);

  //     this.restaurantListService.addOrderItem(auth.phoneNumber, this.restaurant.id, itemID);

  //     this.added++;
  //     // const itemID = document.getElementsByClassName(x.order.restaurant.name)[0]
  //     // .getElementsByClassName('restaurantDistance')[0].innerHTML.toString();
  //   });
  // }

  remove() {
    if (this.added > 0) {
      this.added--;
    }
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000,
  //   });
  //   return await loading.present();
  // }
}
