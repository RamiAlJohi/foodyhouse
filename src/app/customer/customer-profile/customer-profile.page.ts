import { Component, OnInit } from '@angular/core';

import { Customer } from '../../shared/customer';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { AuthService } from '../../services/auth.service';
import { Platform, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { ImageUploadService } from '../../services/image-upload.service';
import { first } from 'rxjs/operators';
import { ChiefService } from '../../services/chief.service';
import { LoadingService } from '../../services/loading.service';
// import { CUSTOMERS } from '../../shared/customers';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.page.html',
  styleUrls: ['./customer-profile.page.scss'],
})
export class CustomerProfilePage implements OnInit {

  customer: Customer;
  constructor(private restaurantListService: RestaurantListService, private authService: AuthService,
              private platform: Platform, private router: Router, private previousRouteService: PreviousRouteService,
              public imageUploadService: ImageUploadService, private chiefService: ChiefService,
              public loadingService: LoadingService) {
    this.platform.backButton.subscribe( () => {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    });
  }

  ngOnInit() {
    this.loadingService.present(5000);
    this.authService.getUser()
    .then(auth => {
      console.log('try: ' + auth.email);
      this.restaurantListService.getCustomer(auth.email)
      .subscribe(customer => {this.customer = customer; this.loadingService.dismiss(); });
    });
  }

  toggle(input, icon) {
    if (input.disabled) { // edit
      input.disabled = false;
      icon.name = 'save';
    } else { // save
      input.disabled = true;
      icon.name = 'create';

      if (input.name === 'firstName') {
        this.restaurantListService.updateCustomerFirstName(this.customer.id, input.value);
      } else if (input.name === 'lastName') {
        this.restaurantListService.updateCustomerLastName(this.customer.id, input.value);
      } else if (input.name === 'mobile') {
        this.restaurantListService.updateCustomerMobile(this.customer.id, input.value);
      } else if (input.name === 'email') {
        this.restaurantListService.updateCustomerEmail(this.customer.id, input.value);
      } else {console.log('WTF...'); }
      // function to edit database
      console.log(input.name, input.value);
    }
  }

  async getImage() {
    await this.imageUploadService.getImage();
    this.upload();
  }

  upload() {
    this.authService.getUser().then( user => {
      this.restaurantListService.getCustomer(user.email).pipe(first()).toPromise().then( deliveryGuy => {
        console.log('wtf' + ' ' + user.email);
        this.loadingService.present(5000);
        const path = this.imageUploadService.uploadFile(user.email);
        path.then(imagePath => {
          this.loadingService.dismiss();
          imagePath.ref.getDownloadURL().then( downloadUrl => {
            // this.chiefService.updateItem({image: downloadUrl}, chief.restaurantID, item.id);

            this.chiefService.updateCustomer({image: downloadUrl}, user.email);
          });
        });
      });
    });
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000
  //   });
  //   return await loading.present();
  // }
}
