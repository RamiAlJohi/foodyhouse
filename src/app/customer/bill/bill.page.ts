import { Component, OnInit } from '@angular/core';

import { ModalController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { Order, ORDERSTATUS } from '../../shared/order';
import { first, map, takeWhile } from 'rxjs/operators';
import { OrderItem } from '../../shared/orderItem';
import { AuthService } from '../../services/auth.service';
import { OrderInfo } from '../../shared/orderInfo';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.page.html',
  styleUrls: ['./bill.page.scss'],
})
export class BillPage implements OnInit {

  restaurantID: string;
  order: Order;
  orderItems: OrderItem[];
  totalPrice = 0;
  deliveryPrice: number;
  orderingSessionID: string;
  minimumDileveryPrice = 15;
  alive = true; // unsub all when set to false
  isLoading = false;

  constructor(private modalController: ModalController, private restaurantListService: RestaurantListService,
              private authService: AuthService, private platform: Platform, public alertController: AlertController,
              public loadingService: LoadingService) {
                this.platform.backButton.subscribe( () => {
                  this.modalController.dismiss();
                });
              }

  ngOnInit() {
    console.log('before');
    this.loadingService.present(5000);
    // this.presentLoading().then(a => console.log('presented'));
      // console.log('presented');
      this.authService.getUser()
      .then(auth => {
        console.log('try: ' + auth.email);
        this.restaurantListService.getCustomer(auth.email)
        .pipe(takeWhile(() => this.alive))
        .subscribe(customer => {
          if (customer.orderingSessionID) {
            this.orderingSessionID = customer.orderingSessionID;
            this.restaurantListService.getOrderSnap(this.orderingSessionID)
            .pipe(takeWhile(() => this.alive))
            .subscribe(order => {
              this.order = order;
              this.restaurantID = order.restaurantID;
              this.restaurantListService.getRestaurantSnap(order.restaurantID)
              .pipe(takeWhile(() => this.alive))
              .subscribe(restaurant => {
                this.deliveryPrice = this.minimumDileveryPrice + (2 *
                  this.calculateDistince(restaurant.location.lat, restaurant.location.lng, customer.location.lat, customer.location.lng));

                this.restaurantListService.getOrderItems(this.orderingSessionID)
                .pipe(takeWhile(() => this.alive))
                .subscribe(orderItems => {
                  this.orderItems = orderItems;
                });

                this.restaurantListService.getOrderItems(this.orderingSessionID)
                .pipe(
                  map((orderItems) => orderItems.map(
                    orderitem => orderitem.item.price * orderitem.quantity
                  )),
                  map(orderItem => {
                    console.log(orderItem);
                    if (orderItem.length > 0) { return orderItem.reduce((acc, value) => (acc += value)); } else { return 0; }
                  })
                )
                .pipe(takeWhile(() => this.alive))
                .subscribe(totalPrice => {
                  (totalPrice !== 0) ? this.totalPrice = totalPrice : this.totalPrice = 0;
                  this.loadingService.dismiss();
                });
              });
            });
          } else {
            this.totalPrice = 0;
            this.deliveryPrice = 0;
            this.loadingService.dismiss();
          }
        });
      }, err => this.loadingService.dismiss());
    // });
  }

  calculateDistince(lat1, lon1, lat2, lon2) {
    console.log(lat1);
    console.log(lon1);
    console.log(lat2);
    console.log(lon2);
    const R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = R * c;
    console.log(Math.round(d));
    return Math.round(d);
  }

  confirm() { // this.orderingSessionID
    this.loadingService.present(5000);
    this.alive = false;
    this.authService.getUser()
    .then(auth => {
      console.log('try: ' + auth.email);
      this.restaurantListService.getCustomer(auth.email)
      .pipe(first()).toPromise().then(customer => {
        if (customer.orderingSessionID) {
          this.restaurantListService.getOrderSnap(customer.orderingSessionID)
          .pipe(first()).toPromise().then(order => {
            this.restaurantListService.getRestaurantSnap(order.restaurantID)
            .pipe(first()).toPromise().then(restuarant => {
              if (restuarant.restaurantStatus === 'open') {
                console.log(ORDERSTATUS[1]);
                console.log(customer.orderingSessionID);
                this.restaurantListService.updateOrderStatus(customer.orderingSessionID, ORDERSTATUS[1])
                .then(statusUpdated => {
                  console.log('updated status');
                  this.restaurantListService.releseSession(auth.email)
                  .then(released => {
                    console.log('relesed');
                    this.close().then(closed =>  this.loadingService.dismiss());
                  });
                });
              } else if (restuarant.restaurantStatus === 'busy') {
                this.presentAlert('مشغول').then(a => this.loadingService.dismiss());
              } else {this.presentAlert('مغلق').then(a => this.loadingService.dismiss()); }
            });
          });
        } else {this.loadingService.dismiss(); }
      });
    });
  }

  remove(orderItem: OrderItem) {
    if (orderItem.quantity > 1) {
      this.restaurantListService.updateQuantity(this.orderingSessionID, orderItem.id, orderItem.quantity - 1);
    } else {
      this.restaurantListService.deleteOrderItem(this.orderingSessionID, orderItem.id);
    }
  }

  close() {
    return this.modalController.dismiss();
  }

  async presentAlert(status: string) {
    const alert = await this.alertController.create({
      header: 'عذرا المطعم ' + status + '..',
      buttons: ['حسنا']
    });

    await alert.present();
  }

  // async presentLoading() {
  //   this.isLoading = true;
  //   return await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000,
  //   }).then(a => {
  //     a.present().then(b => {
  //       console.log('presented');
  //       if (!this.isLoading) { a.dismiss().then(c => console.log('abort presenting'));
  //     }});
  //   });
  // }

  // dismissLoading() {
  //   this.isLoading = false;
  //   return this.loadingController.dismiss().then(a => console.log('dismissed'));
  // }
}
