import { Component, OnInit, ViewChild } from '@angular/core';

import { Order, ORDERSTATUS } from '../../shared/order';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { OrderItem } from '../../shared/orderItem';
import { OrderInfo } from '../../shared/orderInfo';

import { ModalController, Platform, LoadingController, InfiniteScroll } from '@ionic/angular';
import { BillPage } from '../bill/bill.page';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { LoadingService } from '../../services/loading.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  status = ORDERSTATUS;
  tabType: string;

  minimumDileveryPrice = 15;

  orders: Order[]; // = ORDERS;

  // orderingSession: Order; // = CUSTOMERS[0].orderingSession;

  constructor(public modalController: ModalController, private restaurantListService: RestaurantListService,
              private authService: AuthService, private platform: Platform, private router: Router,
              private previousRouteService: PreviousRouteService, public loadingService: LoadingService) {
                this.platform.backButton.subscribe( () => {
                  this.router.navigate([this.previousRouteService.getPreviousUrl()]);
                });
              }

  ngOnInit() {
    this.loadingService.present(5000);
    this.authService.getUser()
    .then(auth => {
      console.log('try: ' + auth.email);

      this.restaurantListService.getCustomer(auth.email)
      .subscribe(customer => {
        // this.restaurantListService.getOrderSnap(customer.orderingSessionID)
        // .subscribe(orderingSession => {
        //   this.orderingSession = orderingSession;
        //   const orderInfo: OrderInfo = {
        //     orderID: this.orderingSession.id,
        //     restaurantName: '',
        //     orderItems: [],
        //     totalPrice: 0
        //   };

        //   this.restaurantListService.getRestaurantSnap(this.orderingSession.restaurantID)
        //   .subscribe(restaurant => {
        //     orderInfo.restaurantName = restaurant.name;

        //     orderInfo.distanceToCustomer =
        //       this.calculateDistince(restaurant.location.lat, restaurant.location.lng,
        //                               customer.location.lat, customer.location.lng);
        //     orderInfo.deliveryPrice = this.minimumDileveryPrice + orderInfo.distanceToCustomer * 2 ;
        //     console.log(orderInfo.deliveryPrice);

        //     this.restaurantListService.getOrderItems(this.orderingSession.id)
        //     .subscribe(orderItems => {
        //       orderInfo.orderItems = orderItems;
        //       orderInfo.totalPrice = this.calculateTotal(orderItems);
        //       console.log(orderInfo);
        //       this.orderingSession.orderInfo = orderInfo;
        //     });
        //   });
        // });

        this.restaurantListService.getOrders(auth.email)
        .subscribe(orders => {
          console.log(orders);
          this.orders = orders;
          for (const i of this.orders) {
            const orderInfo: OrderInfo = {
              orderID: i.id,
              restaurantName: '',
              orderItems: [],
              totalPrice: 0
            };

            this.restaurantListService.getRestaurantSnap(i.restaurantID)
            .subscribe(restaurant => {
              orderInfo.restaurantName = restaurant.name;

              orderInfo.distanceToCustomer =
                this.calculateDistince(restaurant.location.lat, restaurant.location.lng,
                                        customer.location.lat, customer.location.lng);
              orderInfo.deliveryPrice = this.minimumDileveryPrice + orderInfo.distanceToCustomer * 2 ;
              console.log(orderInfo.deliveryPrice);

              this.restaurantListService.getOrderItems(i.id)
              .subscribe(orderItems => {
                orderInfo.orderItems = orderItems;
                orderInfo.totalPrice = this.calculateTotal(orderItems);
                console.log(orderInfo);
                i.orderInfo = orderInfo;
                this.loadingService.dismiss();
              });
            });
          }
        });
      });
    });
  }

  async billModal() {
    if (this.tabType === this.status[0]) {
      const modal = await this.modalController.create({
        component: BillPage,
        componentProps: {  }
      });
      return await modal.present();
    }
  }

  calculateTotal(orderItems: OrderItem[]): number {
    let total = 0;
    for (const i of orderItems) {
      total += (i.quantity * i.item.price);
    }
    return total;
  }

  calculateDistince(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = R * c;
    return Math.round(d);
  }

  // remove(orderItem: OrderItem) {
  //   if (orderItem.quantity > 1) {
  //     this.restaurantListService.updateQuantity(this.orderingSessionID, orderItem.id, orderItem.quantity - 1);
  //   } else {
  //     this.restaurantListService.deleteOrderItem(this.orderingSessionID, orderItem.id);
  //   }
  // }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     message: 'انتظر. . .',
  //     duration: 5000
  //   });
  //   return await loading.present();
  // }
}
