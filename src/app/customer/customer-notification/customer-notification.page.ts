import { Component, OnInit } from '@angular/core';
import { RestaurantListService } from '../../services/restaurant-list.service';
import { AuthService } from '../../services/auth.service';
import { Notification } from '../../shared/notification';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-customer-notification',
  templateUrl: './customer-notification.page.html',
  styleUrls: ['./customer-notification.page.scss'],
})
export class CustomerNotificationPage implements OnInit {

  notifications: Notification[];

  constructor(private restaurantListService: RestaurantListService, private authService: AuthService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.presentLoading();
    this.authService.getUser()
    .then(user => {
      this.restaurantListService.getNotification(user.phoneNumber)
      .subscribe(notifications => {this.notifications = notifications; this.loadingController.dismiss(); });
    });
  }

  addNotify() {
    this.authService.getUser()
    .then(user => {
      const notify: Notification = {
        headline: 'تجربة',
        date: Date.now().toString()
      };
      this.restaurantListService.addNotify(user.phoneNumber, notify);
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'انتظر. . .',
      duration: 5000
    });
    return await loading.present();
  }
}
